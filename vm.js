
var util = require('util');

// String table

var strings = {};

// Function table

var functions = {};

// Table of patterns to match on

var match_patterns = {};

// List of processes

var null_process = {id:0};
var processes = {};
var process_count = 0;
var active_processes = [];
var current_process = null_process;
var next_pid = 0;

// Constants

var ROOT_TIMER_INTERVAL = 2147483647;

var MAX_MAILBOX_SIZE = 128;
var MAX_RUN_CYCLES = 64;

var PROCESS_TERMINATED = 0;
var PROCESS_ACTIVE = 1;
var PROCESS_RECIEVING = 2;

var DEBUG_MODE = (process.argv.indexOf('-d') >= 0);

// Merge two objects...

function extend_object(original, source)
{
	var copy = {};

	Object.keys(original).forEach(function(key)
	{
    	copy[key] = original[key];
	}); 

 	Object.keys(source).forEach(function(key)
	{
    	copy[key] = source[key];
	});    

    return copy;
};

// List of pattern match types

var pattern_types = {
	BIND : 0,
	NUMBER : 1,
	STRING : 2,
	LIST : 3,
	OBJECT : 4,
	VALUE : 5,
	EXPR : 6
};

// The list of opcodes

var opcodes = {
	NOOP : 0,
	BIND : 1,
	BINDG : 2,
	ADD : 3,
	SUB : 4,
	MUL : 5,
	DIV : 6,
	MOD : 7,
	APPEND : 8,
	PREPEND : 9,
	SPAWN : 10,
	OBJECT : 11,
	LIST : 12,
	PUSHN : 13,
	PUSHS : 14,
	PUSHV : 15,
	PUSHF : 16,
	PNULL : 17,
	INVOKES : 18,
	INVOKEI : 19,
	INVOKE : 20,
	EXIT : 21,
	SELF : 22,
	PARENT : 23,
	SEND : 24,
	RECV : 25,
	DEREF : 26,
	SDEREF : 27,

	LT : 28,
	GT : 29,
	GTE : 30,
	LTE : 31,
	EQ : 32,
	NE : 33,
	IF : 34,

	MATCH : 35,

	CONCAT : 36,

	_MAX_ : 37
};

// Global bindings. These will always be available
// in scope to all functions. This acts as a way to provide
// hooks for 'native' non-VM functions via the hook(name,f)
// export.

var global_bindings = {};

// Functions for reading opcodes

var opcode_readers = {};

opcode_readers[opcodes.BIND] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.BINDG] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.OBJECT] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.LIST] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.PUSHN] = function (b,p,o)
{
	p.push(b.readDoubleLE(o));
	return 8;
};

opcode_readers[opcodes.PUSHS] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.PUSHV] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.PUSHF] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.INVOKES] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	p.push(b.readInt16LE(o+2));
	return 4;
};

opcode_readers[opcodes.INVOKEI] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	p.push(b.readInt16LE(o+4));
	return 6;
};

opcode_readers[opcodes.INVOKE] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.SPAWN] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.RECV] = function (b,p,o)
{
	p.push(b.readInt32LE(o+0));
	p.push(b.readInt32LE(o+4));
	p.push(b.readInt32LE(o+8));

	o += 12;

	var x;
	for ( x = 0; x < p[2]; x++ )
	{
		p.push(b.readInt32LE(o));
		o += 4;
	}

	return 12 + (x*4);
};

opcode_readers[opcodes.MATCH] = function (b,p,o)
{
	p.push(b.readInt32LE(o+0));
	p.push(b.readInt32LE(o+4));

	o += 8;

	var x;
	for ( x = 0; x < p[1]; x++ )
	{
		p.push(b.readInt32LE(o));
		o += 4;
	}

	return 8 + (x*4);
};

opcode_readers[opcodes.IF] = function (b,p,o)
{
	p.push(b.readInt32LE(o+0));
	p.push(b.readInt32LE(o+4));

	return 8;
};

// Loads a virtual machine module

var code_buffer = null;
var code_offset = 0;

function load_pattern(id)
{
	var pattern = {};

	var type = code_buffer.readInt8(code_offset);
	code_offset += 1;

	var func = code_buffer.readInt32LE(code_offset);
	code_offset += 4;

	var value = null;

	if ( type == pattern_types.STRING )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.NUMBER )
	{
		value = code_buffer.readDoubleLE(code_offset);
		code_offset += 8;
	}
	else if ( type == pattern_types.VALUE )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.BIND )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.LIST )
	{
		var length = code_buffer.readInt32LE(code_offset);
		code_offset += 4;

		value = [];

		var x;
		for ( x = 0; x < length; x++ )
			value.push(load_pattern(0));
	}
	else if ( type == pattern_types.OBJECT )
	{
		var length = code_buffer.readInt32LE(code_offset);
		code_offset += 4;

		value = [];

		var x;
		for ( x = 0; x < length; x++ )
		{
			var strid = code_buffer.readInt32LE(code_offset);
			code_offset += 4;

			value.push([strings[strid], load_pattern(0)]);		
		}
	}

	return {id:id, type:type, func:func, value:value};
}

function opcode_valid(opcode)
{
	return ( (opcode >= 0) && (opcode < opcodes._MAX_) );
}

function load(buffer, buffer_offset, buffer_length)
{
	code_offset = buffer_offset;
	code_buffer = buffer;

	// Check that buffer has valid signature

	var sig = buffer.readInt32LE(code_offset);
	code_offset += 4;

	if ( sig != module.exports.signature )
	{
		if ( DEBUG_MODE )
			console.log('Invalid module signature: ' + sig);

		return false;
	}

	// Load the string table

	var string_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	var x;
	for ( x = 0; x < string_count; x++ )
	{
		var id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var length = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var str = buffer.toString('binary', code_offset, code_offset+length);
		code_offset += length;

		strings[id] = str;
	}

	// Output strings

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded string table')
		console.log('--------------------------');
		console.log(util.inspect(strings, false, null));
		console.log('');
	}

	// Load the match pattern table

	var match_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	var x;
	for ( x = 0; x < match_count; x++ )
	{
		var id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var pattern = load_pattern(id);
		match_patterns[id] = pattern;
	}

	// Output patterns

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded match pattern table');
		console.log('--------------------------');
		console.log(util.inspect(match_patterns, false, null));
		console.log('');
	}

	// Read in the functions

	var function_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	for ( x = 0; x < function_count; x++ )
	{
		var function_id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var instruction_count = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var param_count = buffer.readInt8(code_offset);
		code_offset++;

		var params = [];

		var p;
		for ( p = 0; p < param_count; p++ )
		{
			params.push(strings[buffer.readInt32LE(code_offset)]);
			code_offset += 4;
		}

		var instructions = [];

		var i;
		for ( i = 0; i < instruction_count; i++ )
		{
			var opcode = buffer.readInt8(code_offset);
			if ( !opcode_valid(opcode) )
			{
				console.log('Invalid opcode:',opcode);
				return false;
			}

			code_offset++;

			var instr = {opcode:opcode, params:null};

			var f = opcode_readers[opcode];
			if ( f )
			{
				instr.params = [];
				code_offset += f(buffer,instr.params,code_offset);
			}

			instructions.push(instr);
		}

		functions[function_id] = {id: function_id, instructions: instructions, params:params};
		functions[function_id].__function = functions[function_id];
	}

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded function table');
		console.log('--------------------------');
		console.log(util.inspect(functions, false, null));
		console.log('');
	}

	// Patch match patterns with actual function references
	// instead of IDs. This is just to avoid an additional
	// map lookup at runtime.

	for ( var k in match_patterns )
	{
		var pattern = match_patterns[k];
		if ( pattern.func )
			pattern.func = functions[pattern.func];
	}

	// All good!

	return true;
}

// Terminates a process and sends a termination message
// to it's parent/supervisor.

function terminate(process, reason)
{
	// Do nothing if process already terminated

	if ( process.state == PROCESS_TERMINATED )
		return;

	// If receiving, make sure timer is cancelled

	var timer = (process.receiving ? process.receiving.timer : null);
	if ( timer )
		clearTimeout(timer);

	// Flag process as terminated and ensure
	// receive state is nullified

	process.state = PROCESS_TERMINATED;
	process.receiving = null;

	// Terminate child processes

	var children = process.children;
	process.children = null;

	var x;
	for ( x = 0; x < children.length; x++ )
	{
		var child = children[x];
		terminate(child, null);
	}

	// Clear child list

	children.length = 0;

	// Remove from process map

	delete processes[process.id];

	// Send message to parent if available

	var parent = process.parent;
	if ( parent )
	{
		if ( reason )
			send(parent, object({terminated:{__process:process}, reason:reason}));

		process.parent = null;
	}

	// Reduce process count

	if ( (process.id == 0) || DEBUG_MODE )
		console.log('Process terminated:', process.id, 'reason:',(reason ? util.inspect(reason, false, null) : 'parent_terminated'));

	process_count--;
}

// Wakes up a process and places it in the active list

function wake(process)
{
	// If already awake, do nothing

	if ( (process.state == PROCESS_ACTIVE) || (process.state == PROCESS_TERMINATED) )
		return;

	// Stop a timer if we have one

	if ( process.receiving.timer )
		clearTimeout(process.receiving.timer);

	// Set process to active state and clear
	// receive data.

	process.state = PROCESS_ACTIVE;
	process.receiving = null;

	// Push the process back onto the active list

	active_processes.push(process);

	if ( DEBUG_MODE )
		console.log('Process awakened:',process.id);

	// Run the VM (If not already running)

	run();
}

// Called by the VM when a process times out while receiving
// messages.

function timeout(process)
{
	// Invoke the timeout function on the process
	// and reset it's state to active.

	invoke(process, process.receiving.timeout, []);
	wake(process);

	return true;
}

// Suspends a process and removes it from the active list
// The process will wait until it has received a message
// matching the provided pattern.

function receive(process, timeout_value, timeout_function, clauses, values)
{
	if ( process.state == PROCESS_RECIEVING )
		return;

	// Ensure timeout value is valid (Must be a number)

	if ( timeout_value !== null )
	{
		if ( typeof timeout_value != 'number' )
		{
			terminate(process, error_message('invalid_timeout_value', timeout_value));
			return false;
		}
	}

	// Setup the receive data for the process

	var receiving = {
		timeout: timeout_function,
		clauses: clauses,
		values: values
	};

	// Match with waiting messages immediately

	var mailbox = process.mailbox;

	var x;
	for ( x = 0; x < mailbox.length; x++ )
	{
		var msg = mailbox[x];

		var res = match_message(receiving, msg);
		if ( res )
		{
			mailbox.splice(x,1);
			invoke(process, res[0], res[1], false);
			return true;
		}
	}

	// Put into receiving state

	process.state = PROCESS_RECIEVING;
	process.receiving = receiving;

	// Activate timer if necessary

	if ( timeout_value )
	{
		receiving.timer = setTimeout(function ()
		{
			if ( DEBUG_MODE )
				console.log('Process timed out on recieve:', process.id);

			timeout(process);
		}, timeout_value);
	}

	// All done!

	return true;
}

function match_pattern_recurse(pattern_params, clause, value, scope)
{
	var ctype = clause.type;
	var cval = clause.value;

	switch ( ctype )
	{
		case pattern_types.STRING:
			if ( value === clause.value )
				return clause.func;
			return false;

		case pattern_types.NUMBER:
			if ( value === clause.value )
				return clause.func;
			return false;

		case pattern_types.BIND:
			pattern_params.push(value);
			if ( scope == 0 )
				return clause.func;
			break;

		case pattern_types.LIST:
			var array = value.__list;
			if ( (array === undefined) || (cval.length != array.length) )
				return false

			var length = cval.length;

			var x;
			for ( x = 0; x < length; x++ )
			{
				if ( match_pattern_recurse(pattern_params, cval[x], array[x], scope+1) === false )
					return false;
			}

			if ( scope == 0 )
				return clause.func;

			break;

		case pattern_types.OBJECT:
			var map = value.__object;
			if ( (map === undefined) || (value.__size != cval.length) )
				return false;

			var length = cval.length;

			var x;
			for ( x = 0; x < length; x++ )
			{
				var entry = cval[x];
				var key = entry[0];
				var val = map[key];

				if ( (val === undefined) || (match_pattern_recurse(pattern_params, entry[1], val, scope+1) === false) )
					return false;
			}

			if ( scope == 0 )
				return clause.func;

			break;
	}
}

function match_pattern(clause, value)
{
	var pattern_params = [];

	var func = match_pattern_recurse(pattern_params, clause, value, 0);
	if ( func )
		return [func, pattern_params];

	return null;
}

// Matches a message to a process mailbox

function match_message(receiving, value)
{
	// Match value with the list of pattern clauses

	var clauses = receiving.clauses;
	var result = null;

	var x;
	for ( x = 0; x < clauses.length; x++ )
	{
		var res = match_pattern(clauses[x], value);
		if ( res )
			return res;
	}

	return null;
}

// Sends a message to the given process ID

function send(process, data)
{
	// Make sure process is alive

	if ( process.state == PROCESS_TERMINATED )
		return false;

	// Make sure mailbox size is not too big

	if ( process.mailbox.length >= MAX_MAILBOX_SIZE )
	{
		terminate(process, object({error:'mailbox_maxed_out'}));
		return false;
	}

	// Match process mailbox with message data
	// If no match can be made it will end
	// up in the mailbox.

	if ( process.state == PROCESS_RECIEVING )
	{
		var response = match_message(process.receiving, data);
		if ( response )
		{
			invoke(process, response[0], response[1], true);
			wake(process);
			return true;
		}
	}

	// Add message to mailbox to be handled later

	process.mailbox.push(data);
	return true;
}

// Spawns a new process and returns the PID

function spawn(parent, function_val, params)
{
	// Make sure params are correct

	if ( !params )
		params = [];

	// Spawn the process with new state

	var pid = next_pid++;
	var process = {
		id: pid,
		params: params,
		frames: [],
		stack: [],
		mailbox: [],
		parent: parent,
		children: [],
		receiving: null,
		state: PROCESS_ACTIVE
	};

	// Add new process to children

	if ( parent )
		parent.children.push(process);

	// Invoke the function on the process

	if ( !invoke(process, function_val, params) )
		return null;

	// Record process ID and new process count

	processes[pid] = process;
	process_count++;

	active_processes.push(process);

	if ( DEBUG_MODE )
		console.log('Spawned process:',pid);

	return {__process:process};
}

// Resolves a symbol by going backwards through
// the stack frames.

function resolve_binding(symbol, frames)
{
	// Try the stack frame

	var x;
	for ( x = frames.length-1; x >= 0; x-- )
	{
		var bindings = frames[x].bindings;

		var tmp = bindings[symbol];
		if ( tmp !== undefined )
			return tmp;
	}

	// Now try the global bindings

	var tmp = global_bindings[symbol];
	if ( tmp !== undefined )
		return tmp;

	// No luck, return undefined...

	return undefined;
}

// Maps function parameters to a binding space

function map_params(bindings, fparams, params)
{
	var x = 0;
	for ( x = 0; x < fparams.length; x++ )
	{
		var str = fparams[x];
		bindings[str] = (x < params.length) ? params[x] : null;
	}

	return bindings;
}

// Maps function parameters to a binding space
// and returns a key name if there is a collision

function map_params_safe(bindings, fparams, params)
{
	var x = 0;
	for ( x = 0; x < fparams.length; x++ )
	{
		var str = fparams[x];
		if ( str in bindings )
			return str;

		bindings[str] = (x < params.length) ? params[x] : null;
	}

	return null;
}

// Invokes a function within a process

function invoke(process, func, params, reuse_bindings)
{
	// Ensure we have a valid function value

	if ( !func )
	{
		terminate(process, error_message('invalid_function', null));
		return false;
	}

	// Native function? If so call the function and
	// push value onto stack immediately.

	var n = func.__native;
	if ( n !== undefined )
	{
		var r = n(process, params);
		if ( r !== undefined )
			process.stack.push(r);

		return true;
	}

	// Get function reference from value

	var f = func.__function;
	if ( f === undefined )
	{
		terminate(process, error_message('non_function_invoke', null));
		return false;
	}

	var frames = process.frames;

	// Roll 'completed' stack frames backwards
	// to conserve memory (Tail call optimization)
	// TODO: Reconsider how binding works with this...

	var before = process.frames.length;
	var bindings = null;
	var count = 0;

	var x;
	for ( x = frames.length-1; x >= 0; x-- )
	{
		var frame = frames[x];

		if ( frame.code_ptr >= frame.func.instructions.length )
		{
			count++
			break;
		}
	}

	// Preserve bindings in lowest scope.

	var bindings = ( x >= 0 ) ? frames[x].bindings : {};

	// Remove the unneeded frames.

	frames.splice(frames.length - count, count);

	// Reuse existing bindings if needed. Otherwise
	// just bind to a new scope.

	if ( reuse_bindings )
	{
		var error_key = map_params_safe(bindings, f.params, params);
		if ( error_key )
		{
			terminate(process, error_message('already_binded', error_key));
			return false;
		}
	}
	else
	{
		bindings = map_params(bindings, f.params, params);
	}

	// Push new stack frame

	var stack_frame = {
		func: f,
		bindings: bindings,
		code_ptr: 0
	};

	// Push frame onto process frame stack

	process.frames.push(stack_frame);

	// All good!

	return true;
}

function error_message(cause, info)
{
	return object({error:object({cause:cause, info:info})});
}

// Evaluates a process based on it's state

function eval_process(process)
{
	// Make sure process is active

	if ( process.state != PROCESS_ACTIVE )
		return;

	currentProcess = process;

	// Get current process state

	var stack = process.stack;
	var frames = process.frames;
	var current_frame = frames[frames.length-1];
	var func = current_frame.func;
	var code_ptr = current_frame.code_ptr;
	var instructions = func.instructions;
	var bindings = current_frame.bindings;

	// Execute the instructions

	var x;
	for ( x = code_ptr; x < instructions.length; x++ )
	{
		var instr = instructions[x];

		switch ( instr.opcode )
		{
			case opcodes.NOOP:
				break;

			case opcodes.PNULL:
				stack.push(null);
				break;

			case opcodes.PUSHN:
				stack.push(instr.params[0]);
				break;

			case opcodes.PUSHS:
				stack.push(strings[instr.params[0]]);
				break;

			case opcodes.PUSHV:
				var symbol = strings[instr.params[0]];
				var val = resolve_binding(symbol, frames);
				if ( val === undefined )
				{
					//console.log('FRAMES', frames);
					terminate(process, error_message('undefined_symbol', symbol));
					return;
				}
				stack.push(val);
				break;

			case opcodes.PUSHF:
				var f = functions[instr.params[0]];
				stack.push({__function:f});
				break;

			case opcodes.ADD:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2+n1);
				break;

			case opcodes.SUB:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2-n1);
				break;

			case opcodes.MUL:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2*n1);
				break;

			case opcodes.DIV:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2/n1);
				break;

			case opcodes.MOD:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2%n1);
				break;

			case opcodes.DEREF:
				var key = stack.pop();
				var obj = stack.pop();
				if ( obj.__object === undefined )
				{
					if ( obj.__list === undefined )
					{
						terminate(process, error_message('non_object_dereference', obj));
						return;
					}

					val = obj.__list[key];
					stack.push(val === undefined ? null : val);
					break;
				}
				var val = obj.__object[key];
				stack.push(val === undefined ? null : val);
				break;

			case opcodes.SDEREF:
				var key = stack.pop();
				var obj = stack.pop();
				if ( obj.__object === undefined )
				{
					if ( obj.__list === undefined )
					{
						terminate(process, error_message('non_object_dereference', obj));
						return;
					}

					var val = obj.__list[key]
					if ( val === undefined )
					{
						terminate(process, error_message('invalid_dereference_index', key));
						return;						
					}
					break;
				}
				var val = obj.__object[key];
				if ( val === undefined )
				{
					terminate(process, error_message('invalid_dereference_key', key));
					return;
				}
				stack.push(val);
				break;			

			case opcodes.INVOKES:
				var symbol = strings[instr.params[0]];
				var param_count = instr.params[1];
				var f = resolve_binding(symbol, frames);
				if ( f === undefined )
				{
					terminate(process, error_message('undefined_symbol', symbol));
					return;
				}
				var params = stack.slice(stack.length-param_count);
				process.stack = stack.slice(0, stack.length-param_count);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.INVOKE:
				var param_count = instr.params[0];
				var f = stack[stack.length-param_count-1];
				var params = stack.slice(stack.length-param_count)
				process.stack = stack.slice(0, stack.length-param_count-1);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.INVOKEI:
				var f = functions[instr.params[0]];
				var param_count = instr.params[1];
				var params = stack.slice(stack.length-param_count);
				process.stack = stack.slice(0, stack.length-param_count);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.SPAWN:
				var param_count = instr.params[0];
				var f = stack[stack.length-param_count];
				var pid = spawn(process, f, stack.slice(stack.length-(param_count-1)));
				process.stack = stack.slice(0, stack.length-param_count-1);
				stack = process.stack;
				stack.push(pid);
				break;

			case opcodes.SELF:
				stack.push({__process:process});
				break;

			case opcodes.PARENT:
				var parent = process.parent;
				stack.push(parent ? {__process:parent} : null);
				break;

			case opcodes.BIND:
				var symbol = strings[instr.params[0]];
				var value = stack.pop();

				//console.log(symbol, value);

				if ( bindings[symbol] !== undefined )
				{
					terminate(process, error_message('already_binded', symbol));
					return;
				}

				bindings[symbol] = value;
				//console.log('FRAMES AFTER BIND', frames);

				break;

			case opcodes.BINDG:
				var symbol = strings[instr.params[0]];
				var value = stack.pop();
				if ( global_bindings[symbol] !== undefined )
				{
					terminate(process, error_message('already_binded', symbol));
					return;
				}
				global_bindings[symbol] = value;
				break;

			case opcodes.EXIT:
				console.log('hey');
				terminate(process, 'exit');
				return;

			case opcodes.LIST:
				var length = instr.params[0];
				var array = stack.splice(stack.length-length);
				stack.push({__list:array});
				break;

			case opcodes.OBJECT:
				var entries = instr.params[0]*2;
				var params = stack.splice(stack.length-entries);
				var obj = {};

				var t;
				for ( t = 0; t < entries; t+=2 )
				{
					var key = params[t+0];
					var value = params[t+1];
					obj[key] = value;
				}

				stack.push(object(obj, instr.params[0]));
				break;

			case opcodes.SEND:
				var message = stack.pop();
				var procv = stack.pop();

				var proc = (procv ? procv.__process : null);
				if ( !proc )
				{
					terminate(process, error_message('invalid_send', procv));
					return;
				}

				send(proc, message, process);
				break;

			case opcodes.LT:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 < v1);
				break;

			case opcodes.GT:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 > v1);
				break;

			case opcodes.LTE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 <= v1);
				break;

			case opcodes.GTE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 >= v1);
				break;

			case opcodes.EQ:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 == v1);
				break;

			case opcodes.NE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 != v1);
				break;

			case opcodes.CONCAT:
				var v1 = stack.pop();
				var v2 = stack.pop();

				if ( typeof v2 === 'object' )
				{
					if ( v1.__list )
						val = list(n2.__list.concat(v1.__list));
					if ( v1.__object )
						val = object(extend_object(v2.__object, v1.__object));
					else
						val = list(v2.__list.concat(v1));
				}
				else
					val = null;

				stack.push(val);
				break;

			case opcodes.RECV:
				var timeout = instr.params[0]; 				// Timeout function ID
				var stack_values = instr.params[1];			// Number of stack values
				var clause_count = instr.params[2]; 		// Number of clauses
				var tmp;
				
				// Get the clauses

				var clauses = [];

				for ( tmp = 0; tmp < clause_count; tmp++ )
					clauses.push(match_patterns[instr.params[3+tmp]]);

				// Pop expression stack values

				var values = [];

				for ( tmp = 0; tmp < stack_values; tmp++ )
					values.push(stack.pop());

				var timeout_value = (timeout >= 0) ? stack.pop() : null;
				var timeout_function = (timeout >= 0) ? functions[timeout] : null;

				current_frame.code_ptr = x+1;
				receive(process, timeout_value, timeout_function, clauses, values);
				return;

			case opcodes.MATCH:
				var stack_values = instr.params[0];			// Number of stack values
				var clause_count = instr.params[1]; 		// Number of clauses
				var tmp;
				
				// Get the clauses

				var clauses = [];

				for ( tmp = 0; tmp < clause_count; tmp++ )
					clauses.push(match_patterns[instr.params[2+tmp]]);

				// Pop the value to match

				var val = stack.pop();

				// Pop expression stack values

				/*var values = [];

				for ( tmp = 0; tmp < stack_values; tmp++ )
					values.push(stack.pop());*/

				current_frame.code_ptr = x+1;

				// Perform the matching on all clauses

				for ( tmp = 0; tmp < clauses.length; tmp++ )
				{
					var pattern_params = [];

					var func = match_pattern_recurse(pattern_params, clauses[tmp], val, 0);
					if ( func )
					{
						invoke(process, func, pattern_params);
						return;
					}
				}
				break;

			case opcodes.IF:
				var func = functions[instr.params[0]];	// Function to call if true
				var jmp = instr.params[1];				// Jump position if true

				var tmp = stack.pop();
				if ( tmp == true )
				{
					current_frame.code_ptr = jmp;
					invoke(process, func, [], true);
					return;
				}
				break;

			default:
				terminate(process, error_message('invalid_instruction', instr.opcode));
				return;
		}
	}

	if ( DEBUG_MODE )
		console.log('Function completed with stack:', process.stack, "frames:", frames);

	// Function completed. Pop current stack frame

	frames.pop();

	// Done with process? If so, terminate it.

	if ( frames.length == 0 )
		terminate(process, 'completed');
}

function object(obj, size)
{
	return {__object:obj, __size:((size !== undefined) ? size : Object.keys(obj).length)};
}

function list(items)
{
	return {__list:items};
}

// Evaluates active processes and builds a new
// active process list after evaluation.
//
// TODO: Come up with better way to
// handle new active process list?

function eval()
{
	// Reset active list. When evaluation is complete,
	// active_processes will have only newly spawned
	// processes from this cycle.

	var current_active = active_processes;
	active_processes = [];

	// Step 1: Evaluate active processes

	current_active.forEach(function (process)
	{
		eval_process(process);
	});

	// Step 2: Determine which processes are still active
	// by filtering out inactive processes after the
	// evaluation has completed.

	current_active.forEach(function (process)
	{
		if ( process.state == PROCESS_ACTIVE )
			active_processes.push(process);
	});

	// All done!

	return true;
}

// Evaluates function 0 to initialize state
// end start proces

var running = false;
var root_timer = null;

function run()
{
	// Can only run once at a time...

	if ( running )
		return false;

	running = true;

	// Keep evaluating until we reached the maximum number of cycles
	// or there are no more active processes.

	var cycles = 0;
	while ( (active_processes.length > 0) && ((cycles++) < MAX_RUN_CYCLES) )
		eval();

	// Did all processes terminate?

	if ( process_count == 0 )
	{
		if ( DEBUG_MODE )
			console.log('Run completed');

		running = false;
		shutdown();

		return true;
	}

	// Did we reach max cycles? If so schedule for another
	// run later.

	if ( cycles >= MAX_RUN_CYCLES )
	{
		if ( DEBUG_MODE )
			console.log('Max cycles reached:',cycles);

		setTimeout(run, 0);
	}

	// Reset current process reference

	current_process = null_process;

	// All done!

	running = false;
	return true;
}

// Shuts down the VM

function shutdown()
{
	if ( running )
		return false;

	// Clear the root timer

	if ( root_timer )
	{
		clearInterval(root_timer);
		root_timer = null;
	}

	// Hard terminate all processes
	// by just resetting the process map
	// and active list.

	strings = {};
	functions = {};
	match_patterns = {};
	active_processes = [];
	process_count = 0;
	processes = {};

	// All done!

	return true;
}

// Bootstraps the VM and begins running

function boot()
{
	// Are we already booted?

	if ( process_count != 0 )
		return false;

	// Create a root timer. This will keep the process
	// alive in case we are using node. Can also use
	// it for monitoring VM stats in future...

	root_timer = setInterval(function () {}, ROOT_TIMER_INTERVAL);

	// Find the root function

	var f = functions[0];
	if ( f === undefined )
		return false;

	// Spawn a root process for the function

	spawn(null, {__function:f}, []);

	// Start running the processes

	run();

	// All good!

	return true;
}

// Registers a function within the main process scope

function hook(name, func)
{
	if ( (typeof name !== 'string') || (typeof func !== 'function') )
		return false;

	global_bindings[name] = {__native:func, __name:name};
	return true;
}

// Exports

module.exports.signature = 0xBEEF;
module.exports.opcodes = opcodes;
module.exports.pattern_types = pattern_types;

module.exports.hook = hook;
module.exports.object = object;

module.exports.send = send;
module.exports.load = load;
module.exports.boot = boot;

