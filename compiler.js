
var util = require('util');
var fs = require('fs');
var vm = require('./vm.js');

var DEBUG_MODE = (process.argv.indexOf('-d') >= 0);

var code_buffer = null;
var code_offset = 0;
var errors = 0;

var breakers = ['+',',','-','!','|','=','!=','==','<','>','>=','<=','++','*','/','\\','\\\\','[',']','{','}','(',')',';',':','.'];
var keywords = ['bind', 'exit', 'recv', 'def', 'then', 'match', 'with', 'mod', 'spawn', 'timeout', 'or', 'if', 'elseif', 'else', 'end'];
var white_space = [' ','\r', '\n', '\t'];
var in_white_space = null;

var patterns = {
	'self' : /^self$/,
	'true' : /^true$/,
	'false' : /^false$/,
	'parent' : /^parent$/,
	'number': /^([0-9]{1}[0-9\.]*)$/,
	'bind': /^\@([a-zA-Z_]{1}[a-zA-Z0-9_\.]*)$/,
	'symbol': /^([a-zA-Z_]{1}[a-zA-Z0-9_\.]*)$/,
};

var pos = 0
var buffer = "";
var escaping = false;
var tokens = [];
var in_str = null;

// Tokenizes the current buffer

function token(type)
{
	if ( buffer == '' )
		return;

	// If type is null, try to deduce it by
	// pattern matching.

	if ( type == null )
	{
		if ( keywords.indexOf(buffer) >= 0 )
		{
			type = buffer;
		}
		else
		{
			// Perform regex match

			for ( var k in patterns )
			{
				var regex = patterns[k];

				var res = buffer.match(regex);
				if ( res )
				{
					buffer = res[1];
					type = k;
					break;
				}
			}
		}
	}

	// Still no type? Then unknown

	if ( type == null )
		type = "unknown";

	// Add token to list

	tokens.push([type, buffer]);
	buffer = '';
}

// Handles string content

function process_string(next)
{
	if ( escaping )
	{
		buffer += next;
		escaping = false;
	}
	else if ( next == '\\' )
	{
		escaping = true;
	}
	else if ( next == in_str )
	{
		token('string');
		in_str = null;
	}
	else
		buffer += next;
}

// Main parser

function tokenize(code)
{
	var last = '';

	while (pos < code.length )
	{
		// Get next character

		var next = code.charAt(pos++);

		// Are we in a string?

		if ( in_str )
		{
			process_string(next);
			continue;
		}

		// Eat comments

		if ( next == '%' )
		{
			do
			{
				next = code.charAt(pos++);
			}
			while ( (next != '') && (next != '\n') )
		}

		// Entering a string?

		if ( (next == "\"") || (next == "\'") )
		{
			in_str = next;
			continue;
		}

		// If we are not in white space and have hit
		// an open parenthesis, then we are doing
		// an invokation.

		if ( (pos > 0) && (white_space.indexOf(last) < 0) && (next == '(') && (last != '(') )
		{
			token();
			buffer = '(';
			token('invoke');
			buffer = '';
			last = next;
			continue;
		}

		last = next;

		// Handle white space

		var is_ws = (white_space.indexOf(next) >= 0);

		if ( is_ws !== in_white_space )
		{
			if ( is_ws )
			{
				token();
				buffer = '';
				continue;
			}

			in_white_space = is_ws;
		}

		// Will this character break the sequence?

		if ( (next == '.') && /^[0-9]+$/.test(buffer.charAt(0)) )
		{
			buffer += '.';
			continue;
		}

		var test = next+code.charAt(pos);

		if ( breakers.indexOf(test) >= 0 )
		{
			token();
			buffer = test;
			token(test);
			pos++;
			continue;
		}
		else if ( breakers.indexOf(next) >= 0 )
		{
			token();
			buffer = next;
			token(next);
			continue;
		}

		// Add to buffer

		buffer += next;
	}

	token();

	return tokens;
}

// Parser

var operators = ['+', '-', '/', '||', '\\', '\\\\', '*', '++', '!', '==', '!=', '<', '>', '<=', '>='];
var tokens = [];
var next_token = 0;
var errors = 0;

function error(err)
{
	console.log(err);
	errors++;
}

function consume_token()
{
	return tokens[next_token++];
}

function rewind_token()
{
	next_token--;
	if ( next_token < 0 )
		next_token = 0;
}

function peek_token()
{
	if ( next_token >= tokens.length )
		return null;

	return tokens[next_token];
}

function parse_object()
{
	// Object definition

	var entries = [];
	var key = null;
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( (type == 'string') || (type == 'symbol') )
		{
			if ( key )
			{
				rewind_token();
				left = parse_expression();
			}
			else
				key = token;
		}
		else if ( type == 'bind' )
		{
			// Accept a bind within a pattern match...

			left = {op:'bind', symbol:token[1]};
		}
		else if ( type == ':' )
		{
			if ( !key )
				error('Syntax error. Unexpected ";"');

			continue;
		}
		else if ( type == '}' )
		{
			// End of object definition

			break;
		}
		else if ( type == ',' )
		{
			if ( !left )
				error('Syntax error. Unexpected ","');

			// End of key/value pair

			entries.push({key:key, value:left});
			key = null;
		}
		else
		{
			if ( !key )
				error('Syntax error. Malformed object definition!');

			rewind_token();
			left = parse_expression();
		}
	}

	if ( left )
		entries.push({key:key, value:left});

	return {op:'object', entries:entries};
}

function parse_invoke_parameters()
{
	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid parameter list');

			params.push(left);
			left = null;
		}
		else if ( type == ')' )
		{
			// End of param list

			break;
		}
		else
		{
			rewind_token();
			left = parse_expression('invoke');
		}
	}

	if ( !token || token[0] != ')' )
		error('Syntax error: unclosed param list.')

	if ( left )
		params.push(left);

	return params;	
}

function parse_param_list()
{
	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid parameter definition list');

			params.push(left);
			left = null;
		}
		else if ( type == ')' )
		{
			// End of param list

			break;
		}
		else if ( type == 'symbol' )
		{
			left = token[1];
		}
		else
			error('Syntax error: Invalid parameter list');
	}

	if ( left )
		params.push(left);

	return params;
}

function parse_array()
{
	// Array definition

	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid array definition');

			params.push(left);
		}
		else if ( type == '|' )
		{
			if ( !left )
				error('Syntax error: "' + token[1] + '"');

			var right = parse_expression();
			return {op:'prepend', left:left, right:right};
		}
		else if ( type == 'bind' )
		{
			// Accept a bind within a pattern match...

			left = {op:'bind', symbol:token[1]};
		}
		else if ( type == ']' )
		{
			// End of array

			break;
		}
		else
		{
			rewind_token();
			left = parse_expression();
		}
	}

	if ( left )
		params.push(left);

	return {op:'list', entries:params};
}

function parse_recv()
{
	var patterns = [];
	var timeout = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == 'timeout' )
		{
			if ( timeout )
				error('Multiple "timeout" branches in "recv"');

			var expr = parse_expression();
			var block = parse_block();

			timeout = {expression:expr, block:block};
		}
		else if ( type == 'end' )
		{
			// End of recv block

			break;
		}
		else
		{
			if ( type == 'or' )
			{
				if ( patterns.length == 0 )
					error('Syntax error: ' + type);
			}
			else
				rewind_token();

			var pattern = parse_expression('pattern');

			var block = parse_block();
			block.pattern = pattern;

			patterns.push(block);
		}
	}

	return {op:'recv', patterns:patterns, timeout:timeout};
}

function parse_match()
{
	var patterns = [];
	var any = null;
	var timeout = null;
	var expr = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == 'end' )
		{
			// End of recv block

			break;
		}
		else if ( type == 'with' )
		{
			if ( expr == null )
				error('Invalid expression for match');
		}
		else
		{
			if ( type == 'or' )
			{
				if ( patterns.length == 0 )
					error('Syntax error: ' + type);
			}
			else
				rewind_token();

			if ( expr == null )
			{
				expr = parse_expression();
				continue;
			}

			var pattern = parse_expression('pattern');

			var block = parse_block();
			block.pattern = pattern;

			patterns.push(block);
		}
	}

	if ( expr == null )
		error('No expression to match');

	return {op:'match', expr:expr, patterns:patterns};
}

function parse_if()
{
	var cases = [];
	var any = null;
	var timeout = null;
	var def = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == 'end' )
		{
			// End of recv block

			break;
		}
		else if ( type == 'then' )
		{
			if ( expr == null )
				error('Invalid expression for match');
		}
		else
		{
			if ( type == 'elseif' )
			{
				if ( def != null )
					error('Syntax error: ' + type);
				if ( cases.length == 0 )
					error('Syntax error: ' + type);
			}
			else if ( type == 'else' )
			{
				if ( def != null )
					error('Syntax error: else');
			}
			else
				rewind_token();

			var expr = parse_expression();
			var block = parse_block();

			cases.push({expr:expr, block:block});
		}
	}

	if ( cases.length == 0 )
		error('No cases in if statement');

	return {op:'if', cases:cases, default:def};
}

function parse_expression(hint)
{
	var operator = null;
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( !left )
		{
			if ( type == '{' )
				left = parse_object();
			else if ( type == '[' )
				left = parse_array();
			else if ( (type == 'number') || (type == 'string') || (type == 'symbol') || (type == 'true') || (type == 'false') )
				left = {op:'value', value:token};
			else if ( (type == 'bind') && (hint == 'pattern') )
				left = {op:'bind', symbol:token[1]};
			else if ( (type == 'self') || (type == 'parent') || (type == 'spawn') || (type == 'exit') )
				left = {op:type, value:type};
			else if ( type == 'def' )
			{
				var tmp = consume_token();
				var params = [];

				if ( (tmp[0] == '(') || (tmp[0] == 'invoke') )
					params = parse_param_list();
				else
					rewind_token();

				// Parse the function block

				var block = parse_block();
				left = {op:'def', params:params, block:block};

				// Consume the 'end' token

				var token = consume_token();
				if ( token[0] != 'end' )
					error('Syntax error: ' + token[0]);
			}
			else if ( type == 'recv' )
			{
				left = parse_recv();
			}
			else if ( type == 'match' )
			{
				left = parse_match();
			}
			else if ( type == 'if' )
			{
				left = parse_if();
			}
			else if ( type == '(' )
			{
				// Grouping of statements

				left = parse_expression('statement');
			}
			else
			{
				error('Syntax error in expression: "' + token[1] + '"');
			}

			if ( operator )
			{
				// Check for invoke on right side of
				// operator...

				var t = peek_token();
				if ( t && t[0] == 'invoke' )
				{
					if ( type != 'symbol' )
					{
						error('Syntax error: ' + token[1]);
						continue;
					}

					consume_token();

					var params = parse_invoke_parameters();

					var op = (left && (left.op == 'spawn')) ? 'spawn' : 'invoke';
					left = {op:op, name:left.value[1], left:null, params:params};				
				}

				operator.right = left;
				left = operator;
				operator = null;
			}
		}
		else if ( type == ')' )
		{
			if ( hint == 'invoke' )
				rewind_token();

			break;
		}
		else if ( operators.indexOf(type) >= 0 )
		{
			if ( operator )
				error('Syntax error: ' + token[0]);

			operator = {op:type, left:left, right:null};
			left = null;
		}
		else if ( type == 'invoke' )
		{
			// Invocation...

			var right = parse_invoke_parameters();
			var name = null;

			// Allow for optimized call for invocations on
			// direct symbols.

			if ( (left.op == 'value') && (left.value[0] == 'symbol') )
			{
				name = left.value[1];
				left = null;
			}

			// Spawn is a special internal function that compiles
			// to a byte code, so check for that. Otherwise use
			// normal invoke command.

			var op = (left && (left.op == 'spawn')) ? 'spawn' : 'invoke';
			left = {op:op, name:name, left:left, params:right};
		}
		else if ( type == ':' )
		{
			if ( !left || (left.value[0] != 'symbol')  )
				error('Syntax error: Binding to non-symbol')

			var right = parse_expression();
			left = {op:'bind', symbol:left.value[1], right:right};
		}
		else
		{
			rewind_token();
			break;
		}
	}

	return left;
}

var block_depth = 0;

function parse_block()
{
	var statements = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];
		var value = token[1];

		if ( (type == 'end') || (type == 'timeout') || (type == 'or') || (type == 'else') || (type == 'elseif') )
		{
			if ( block_depth == 0 )
			{
				error('Syntax error: ' + type);
				break;
			}

			rewind_token();
			break;
		}
		else
		{
			block_depth++;
			rewind_token();
			left = parse_expression();
			statements.push(left);
			left = null;
			block_depth--;
		}
	}

	if ( left )
		statements.push(left);

	//if ( statements.length == 0 )
	//	return null;

	return {op:'block', statements:statements};
}

function parse(t)
{
	next_token = 0;
	tokens = t;
	
	var statements = [];

	var token;
	while ( next_token < tokens.length )
	{
		var block = parse_block();
		if ( block )
			statements.push(block);
	}

	return {op:'block', statements:statements};
}

// Compiler

var string_table = {};
var match_patterns = {};
var function_buffers = {0:{id:0, code:[], params:[]}};
var current_function = function_buffers[0];
var pattern_id = 1;
var function_id = 1;

function resolve_string(str)
{
	var k;
	for ( k in string_table )
	{
		var tmp = string_table[k];
		if ( str == tmp )
			return parseInt(k);
	}

	if ( !k )
		k = 0;

	var nid = ++k;
	string_table[nid] = str;

	return parseInt(nid);
}

function emit(code)
{
	current_function.code.push(code)
}

function instruction(index)
{
	return current_function.code[index];
}

function current_code_pointer()
{
	return current_function.code.length;
}

var stack_values = 0;

function compile_match_pattern(node, params, level)
{
	var pattern = node.pattern;
	var op = pattern.op;
	var type = null;
	var val = null;

	if ( op == 'value' )
	{
		var ttype = pattern.value[0];
		var tval = pattern.value[1];

		if ( ttype == 'string' )
		{
			val = resolve_string(tval);
			type = vm.pattern_types.STRING;
		}
		else if ( ttype == 'number' )
		{
			val = tval;
			type = vm.pattern_types.NUMBER;
		}
		else if ( ttype == 'symbol' )
		{
			val = resolve_string(tval);
			type = vm.pattern_types.VALUE;
		}
	}
	else if ( op == 'bind' )
	{
		val = resolve_string(pattern.symbol);
		type = vm.pattern_types.BIND;
		params.push(val);
	}
	else if ( op == 'list' )
	{
		val = [];

		var x;
		for ( x = 0; x < pattern.entries.length; x++ )
			val.push(compile_match_pattern({pattern:pattern.entries[x]}, params, level+1));

		type = vm.pattern_types.LIST;
	}
	else if ( op == 'object' )
	{
		val = [];

		var x;
		for ( x = 0; x < pattern.entries.length; x++ )
		{
			var entry = pattern.entries[x];
			val.push({key:resolve_string(entry.key[1]), pattern:compile_match_pattern({pattern:entry.value}, params, level+1)})
		}

		type = vm.pattern_types.OBJECT;
	}
	else
	{
		// Expression...

		compile({op:'block', statements:[pattern]}, []);
		type = vm.pattern_types.EXPR;
		stack_values++;
	}

	// Compile clause function

	var func = (level == 0) ? compile_function(node, params) : 0;
	var id = (level == 0) ? pattern_id++ : 0;

	var self = {id:id, type:type, value:val, params:params, func:func};
	if ( level == 0 )
		match_patterns[id] = self;

	return self;
}

function compile(node)
{
	if ( !node )
		return;

	var op = node.op;

	if ( op == 'bind' )
	{
		compile(node.right);

		emit([(current_function.id == 0 ? 'BINDG' : 'BIND'), resolve_string(node.symbol)]);
	}
	else if ( op == '!' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SEND']);
	}
	else if ( op == '+' )
	{
		compile(node.left);
		compile(node.right);
		emit(['ADD']);
	}
	else if ( op == '-' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SUB']);
	}
	else if ( op == '*' )
	{
		compile(node.left);
		compile(node.right);
		emit(['MUL']);
	}
	else if ( op == '/' )
	{
		compile(node.left);
		compile(node.right);
		emit(['DIV']);
	}
	else if ( op == '==' )
	{
		compile(node.left);
		compile(node.right);
		emit(['EQ']);
	}
	else if ( op == '!=' )
	{
		compile(node.left);
		compile(node.right);
		emit(['NE']);
	}
	else if ( op == '<' )
	{
		compile(node.left);
		compile(node.right);
		emit(['LT']);
	}
	else if ( op == '>' )
	{
		compile(node.left);
		compile(node.right);
		emit(['GT']);
	}
	else if ( op == '<=' )
	{
		compile(node.left);
		compile(node.right);
		emit(['LTE']);
	}
	else if ( op == '>=' )
	{
		compile(node.left);
		compile(node.right);
		emit(['GTE']);
	}
	else if ( op == '++' )
	{
		compile(node.left);
		compile(node.right);
		emit(['CONCAT']);
	}
	else if ( op == 'mod' )
	{
		compile(node.left);
		compile(node.right);
		emit(['MOD']);
	}
	else if ( op == '\\' )
	{
		compile(node.left);
		compile(node.right);
		emit(['DEREF']);
	}
	else if ( op == '\\\\' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SDEREF']);
	}
	else if ( op == 'exit' )
	{
		emit(['EXIT']);
	}
	else if ( op == 'self' )
	{
		emit(['SELF']);
	}
	else if ( op == 'parent' )
	{
		emit(['PARENT']);
	}
	else if ( op == 'spawn' )
	{
		node.params.forEach(function (e)
		{
			if ( e )
				compile(e);
		});

		emit(['SPAWN', node.params.length]);
	}
	else if ( op == 'block' )
	{
		node.statements.forEach(function (e)
		{
			if ( e )
				compile(e);
		});
	}
	else if ( op == 'invoke' )
	{
		if ( node.left )
			compile(node.left);

		node.params.forEach(function (e)
		{
			if ( e )
				compile(e);
		});

		if ( node.name )
			emit(['INVOKES', resolve_string(node.name), node.params.length]); // Invoke with named symbol
		else
			emit(['INVOKE', node.params.length]); // Invoke off stack
	}
	else if ( op == 'object' )
	{
		node.entries.forEach(function (e)
		{
			emit(['PUSHS', resolve_string(e.key[1])]);
			compile(e.value);
		});

		emit(['OBJECT',node.entries.length]);
	}
	else if ( op == 'list' )
	{
		node.entries.forEach(function (e)
		{
			compile(e);
		});

		emit(['LIST', node.entries.length]);
	}
	else if ( op == 'prepend' )
	{
		compile(node.left);
		compile(node.right);
		emit(['PREPEND']);
	}
	else if ( op == '++' )
	{
		compile(node.left);
		compile(node.right);
		emit('APPEND');
	}
	else if ( op == 'value' )
	{
		var token = node.value;
		var type = token[0];
		var value = token[1];

		if ( type == 'string' )
			emit(['PUSHS', resolve_string(value)]);
		else if ( type == 'number' )
			emit(['PUSHN', value]);
		else if ( type == 'symbol' )
			emit(['PUSHV', resolve_string(value)]);
		else if ( type == 'self' )
			emit(['SELF']);

	}
	else if ( op == 'recv' )
	{
		var timeout = -1;
		var clauses = -1;

		// Check for timeout clause

		if ( node.timeout )
		{
			timeout = compile_function(node.timeout.block);
			compile(node.timeout.expression);
		}

		// Compile pattern clauses

		var ins = ['RECV', timeout, 0, node.patterns.length];

		stack_values = 0;

		node.patterns.forEach(function (p)
		{
			var pattern = compile_match_pattern(p, [], 0);
			ins.push(pattern.id);
		});

		ins[2] = stack_values;

		// Setup instruction

		emit(ins);
	}
	else if ( op == 'match' )
	{
		// Emit the instructions for expression

		compile(node.expr);

		// Compile pattern clauses

		var ins = ['MATCH', 0, node.patterns.length];

		stack_values = 0;

		node.patterns.forEach(function (p)
		{
			var pattern = compile_match_pattern(p, [], 0);
			ins.push(pattern.id);
		});

		ins[1] = stack_values;

		// Setup instruction

		emit(ins);
	}
	else if ( op == 'if' )
	{
		var funcs = [];

		// Compile the expressions

		var patchLocs = [];

		node.cases.forEach(function (c)
		{
			var func = compile_function(c.block, []);

			compile(c.expr);
			patchLocs.push(current_code_pointer());

			emit(['IF', func, 0]);
		});

		// Get the end point

		var end = current_code_pointer();

		// Patch the jump locations

		patchLocs.forEach(function (loc)
		{
			var ins = instruction(loc);
			ins[2] = end;
		});
	}
	else if ( op == 'def' )
	{
		// Compile the function

		var new_function_id = compile_function(node.block, node.params);
		
		// Push new function ID onto stack

		emit(['PUSHF',new_function_id]);
	}
}

function compile_function(block, params)
{
	if ( !params )
		params = [];

	// Map the parameter names to the string table

	var params = params.map(function (n)
	{
		if ( typeof n === 'number' )
			return n;

		return resolve_string(n);
	});

	// Generate new function entry

	var old_function = current_function;
	var new_function_id = function_id++;
	function_buffers[new_function_id] = {code:[], params:params, id:new_function_id};
	current_function = function_buffers[new_function_id];

	// Compile function code into new buffer

	if ( block )
		compile(block);

	// Reset to old buffer

	current_function = old_function;

	// Return function ID

	return new_function_id;	
}

// Assembler ///////////////////////////////////

var op_codes = {
	'NOOP': [vm.opcodes.NOOP, null],
	'BIND': [vm.opcodes.BIND, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'BINDG' : [vm.opcodes.BINDG, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'ADD': [vm.opcodes.ADD, null],
	'SUB': [vm.opcodes.SUB, null],
	'MUL': [vm.opcodes.MUL, null],
	'DIV': [vm.opcodes.DIV, null],
	'MOD': [vm.opcodes.MOD, null],
	'LT': [vm.opcodes.LT, null],
	'GT': [vm.opcodes.GT, null],
	'LTE': [vm.opcodes.LTE, null],
	'GTE': [vm.opcodes.GTE, null],
	'EQ': [vm.opcodes.EQ, null],
	'NE': [vm.opcodes.NE, null],
	'CONCAT': [vm.opcodes.CONCAT, null],
	'APPEND': [vm.opcodes.APPEND, null],
	'PREPEND': [vm.opcodes.PREPEND, null],
	'SPAWN': [vm.opcodes.SPAWN, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'OBJECT': [vm.opcodes.OBJECT, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'LIST': [vm.opcodes.LIST, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'PUSHN': [vm.opcodes.PUSHN, function (b,p,o) { b.writeDoubleLE(p[0], o); return 8; }],
	'PUSHS': [vm.opcodes.PUSHS, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'PUSHV': [vm.opcodes.PUSHV, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'PUSHF': [vm.opcodes.PUSHF, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'PNULL': [vm.opcodes.PNULL, null],
	'INVOKES': [vm.opcodes.INVOKES, function (b,p,o) { b.writeInt16LE(p[0], o); b.writeInt16LE(p[1], o+2); return 4; }],
	'INVOKE': [vm.opcodes.INVOKE, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'INVOKEI': [vm.opcodes.INVOKEI, function (b,p,o) { b.writeInt32LE(p[0], o); b.writeInt16LE(p[1],o+4); return 6; }],
	'EXIT': [vm.opcodes.EXIT, null],
	'SELF': [vm.opcodes.SELF, null],
	'DEREF' : [vm.opcodes.DEREF, null],
	'SDEREF' : [vm.opcodes.SDEREF, null],
	'PARENT': [vm.opcodes.PARENT, null],
	'SEND': [vm.opcodes.SEND, null],
	'RECV': [vm.opcodes.RECV, function (b,p,o)
	{
		b.writeInt32LE(p[0], o+0);
		b.writeInt32LE(p[1], o+4);
		b.writeInt32LE(p[2], o+8);

		o += 12;

		var x;
		for ( x = 0; x < p[2]; x++ )
		{
			b.writeInt32LE(p[3+x], o);
			o += 4;
		}

		return 12 + (x*4);
	}],
	'MATCH': [vm.opcodes.MATCH, function (b,p,o)
	{
		b.writeInt32LE(p[0], o+0);
		b.writeInt32LE(p[1], o+4);

		o += 8;

		var x;
		for ( x = 0; x < p[1]; x++ )
		{
			b.writeInt32LE(p[2+x], o);
			o += 4;
		}

		return 8 + (x*4);
	}],
	'IF': [vm.opcodes.IF, function (b,p,o)
	{
		b.writeInt32LE(p[0], o+0);
		b.writeInt32LE(p[1], o+4);

		return 8;
	}]
};

function assemble_pattern(pattern)
{
	var type = pattern.type;

	code_buffer.writeInt8(type, code_offset);
	code_offset += 1;

	code_buffer.writeInt32LE(pattern.func, code_offset);
	code_offset += 4;

	if ( type == vm.pattern_types.STRING )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.NUMBER )
	{
		code_buffer.writeDoubleLE(pattern.value, code_offset);
		code_offset += 8;
	}
	else if ( type == vm.pattern_types.VALUE )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.BIND )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.LIST )
	{
		var arr = pattern.value;

		code_buffer.writeInt32LE(arr.length, code_offset);
		code_offset += 4;

		var x;
		for ( x = 0; x < arr.length; x++ )
			assemble_pattern(arr[x]);
	}
	else if ( type == vm.pattern_types.OBJECT )
	{
		var entries = pattern.value;

		code_buffer.writeInt32LE(entries.length, code_offset);
		code_offset += 4;

		var x;
		for ( x = 0; x < entries.length; x++ )
		{
			var entry = entries[x];
			
			code_buffer.writeInt32LE(entry.key, code_offset);
			code_offset += 4;

			assemble_pattern(entry.pattern);
		}
	}
}

function assemble(func)
{
	// Output function ID

	code_buffer.writeInt32LE(func.id,code_offset);
	code_offset += 4;

	// Output instruction count

	code_buffer.writeInt32LE(func.code.length,code_offset);
	code_offset += 4;

	// Output parameter count

	code_buffer.writeInt8(func.params.length,code_offset);
	code_offset++;

	// Output parameter names (Referenced in string table)

	func.params.forEach(function (p)
	{
		code_buffer.writeInt32LE(p,code_offset);
		code_offset += 4;
	});

	// Output the list of instructions

	func.code.forEach(function (instruction)
	{
		var name = instruction[0];
		var params = instruction.slice(1);

		var opcode = op_codes[name];
		if ( !opcode )
			error('Invalid instruction: ' + name);

		code_buffer.writeInt8(opcode[0],code_offset);
		code_offset++;

		if ( opcode[1] )
			code_offset += opcode[1](code_buffer,params,code_offset);
	});
}

// Compiles a block of code and runs the
// opcodes on the VM.

function xy_compile(input)
{
	code_buffer = new Buffer(4096*4, 'binary');
	code_offset = 0;
	errors = 0;

	code_buffer.writeInt32LE(vm.signature, code_offset);
	code_offset += 4;

	// Output code tree

	var tree = parse(tokenize(input.toString()));

	if ( DEBUG_MODE )
	{
		console.log(util.inspect(tree, false, null));
		console.log('');
	}

	compile(tree);

	// Output string table

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('String table')
		console.log('--------------------------');
		console.log(util.inspect(string_table, false, null));
		console.log('');
	}

	code_buffer.writeInt32LE(Object.keys(string_table).length,code_offset);
	code_offset += 4;

	for ( var k in string_table )
	{
		var s = string_table[k];

		code_buffer.writeInt32LE(k, code_offset);
		code_offset += 4;

		code_buffer.writeInt32LE(s.length,code_offset);
		code_offset += 4;

		code_buffer.write(s,code_offset,s.length,'binary');
		code_offset += s.length;
	}

	// Output match pattern table

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Match pattern table')
		console.log('--------------------------');
		console.log(util.inspect(match_patterns, false, null));
		console.log('');
	}

	code_buffer.writeInt32LE(Object.keys(match_patterns).length, code_offset);
	code_offset += 4;

	for ( var k in match_patterns )
	{
		var pattern = match_patterns[k];

		// Output pattern ID

		code_buffer.writeInt32LE(pattern.id, code_offset);
		code_offset += 4;

		// Output the pattern

		assemble_pattern(pattern);
	}

	// Assemble the functions to the buffer

	code_buffer.writeInt32LE(Object.keys(function_buffers).length,code_offset);
	code_offset += 4;

	for ( var k in function_buffers )
	{
		var func = function_buffers[k];

		if ( DEBUG_MODE )
		{
			console.log('--------------------------');
			console.log('Function ' + k);
			console.log('--------------------------');	
			console.log(util.inspect(func, false, null));
			console.log('');
		}

		assemble(func);
	}

	// Add some bindings for testing

	vm.hook('inspect', function (process, params)
	{
		var processed = params.map(function (param)
		{
			if ( param == null )
				return '<<null>>';
			else if ( typeof param == 'number' )
				return '<<number:' + param + '>>';
			else if ( typeof param == 'string' )
				return '<<string:"' + param + '">>';
			else if ( typeof param == 'object' )
			{
				if ( param.__function )
					return '<<function:' + param.__function.id + '>>';
				else if ( param.__process )
					return '<<process:' + param.__process.id + '>>';
				else if ( param.__native )
					return '<<native:' + param.__name + '>>';
				else if ( param.__object )
					return '<<object:' + Object.keys(param.__object).join(',') + '>>';
				else if ( param.__list )
					return '<<list:' + param.__list.length + '>>';
			}
			else
				return '<<unknown>>';
		});

		console.log(processed.join(' '));
	});

	vm.hook('length', function (process, params)
	{
		if ( params.length > 0 )
		{
			var param = params[0];

			if ( param.__object )
				return param.__size;
			else if ( param.__list )
				return param.__list.length;
		}

		return 0;		
	});

	vm.hook('log', function (process, params)
	{
		var mapped = params.map(function (param)
		{
			if ( param && (typeof param === 'object') )
			{
				if ( param.__function )
					return '<<function:' + param.__function.id + '>>';
				else if ( param.__process )
					return '<<process:' + param.__process.id + '>>';
				else if ( param.__native )
					return '<<native:' + param.__name + '>>';
				else if ( param.__object )
					return '<<object:{' + Object.keys(param.__object).join(',') + '}>>';
				else if ( param.__list )
					return '<<list:[' + param.__list.join(',') + ']>>';
			}
			else if ( param == null )
				return '<<null>>'
			else
				return param;
		});

		console.log(mapped.join(' '));
	});
}

// External hook function

function xy_hook(name, func)
{
	vm.hook(name, func);
}

// Boots the VM and starts running code

function xy_boot()
{
	if ( errors )
		return false;
	
	vm.load(code_buffer, 0, code_offset);
	vm.boot();

	return true;
}

// Run from command line if available. Otherwise run from DOM

if ( process.argv.length > 2 )
{
	xy_compile(fs.readFileSync(process.argv[2]));
	return xy_boot();
}
else if ( window )
{
	window.addEventListener('load', function() {
		var elements = document.querySelectorAll('script[type="text/xy"]');
		var content = '';

		var x;
		for ( x = 0; x < elements.length; x++ )
			content += elements[x].innerText;

		xy_compile(content);
	}, false);
}

window.xy_boot = xy_boot;
window.xy_hook = xy_hook;
