(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (process,Buffer){

var util = require('util');
var fs = require('fs');
var vm = require('./vm.js');

var DEBUG_MODE = (process.argv.indexOf('-d') >= 0);

var code_buffer = null;
var code_offset = 0;
var errors = 0;

var breakers = ['+',',','-','!','|','=','!=','<','>','>=','<=','++','*','/','\\','\\\\','[',']','{','}','(',')',';',':','.'];
var keywords = ['bind', 'exit', 'recv', 'def', 'any', 'mod', 'spawn', 'timeout', 'or', 'if', 'end'];
var white_space = [' ','\r', '\n', '\t'];
var in_white_space = null;

var patterns = {
	'self' : /^self$/,
	'true' : /^true$/,
	'false' : /^false$/,
	'parent' : /^parent$/,
	'number': /^([0-9]{1}[0-9\.]*)$/,
	'bind': /^\@([a-zA-Z_]{1}[a-zA-Z0-9_\.]*)$/,
	'symbol': /^([a-zA-Z_]{1}[a-zA-Z0-9_\.]*)$/,
};

var pos = 0
var buffer = "";
var escaping = false;
var tokens = [];
var in_str = null;

// Tokenizes the current buffer

function token(type)
{
	if ( buffer == '' )
		return;

	// If type is null, try to deduce it by
	// pattern matching.

	if ( type == null )
	{
		if ( keywords.indexOf(buffer) >= 0 )
		{
			type = buffer;
		}
		else
		{
			// Perform regex match

			for ( var k in patterns )
			{
				var regex = patterns[k];

				var res = buffer.match(regex);
				if ( res )
				{
					buffer = res[1];
					type = k;
					break;
				}
			}
		}
	}

	// Still no type? Then unknown

	if ( type == null )
		type = "unknown";

	// Add token to list

	tokens.push([type, buffer]);
	buffer = '';
}

// Handles string content

function process_string(next)
{
	if ( escaping )
	{
		buffer += next;
		escaping = false;
	}
	else if ( next == '\\' )
	{
		escaping = true;
	}
	else if ( next == in_str )
	{
		token('string');
		in_str = null;
	}
	else
		buffer += next;
}

// Main parser

function tokenize(code)
{
	var last = '';

	while (pos < code.length )
	{
		// Get next character

		var next = code.charAt(pos++);

		// Are we in a string?

		if ( in_str )
		{
			process_string(next);
			continue;
		}

		// Eat comments

		if ( next == '%' )
		{
			do
			{
				next = code.charAt(pos++);
			}
			while ( (next != '') && (next != '\n') )
		}

		// Entering a string?

		if ( (next == "\"") || (next == "\'") )
		{
			in_str = next;
			continue;
		}

		// If we are not in white space and have hit
		// an open parenthesis, then we are doing
		// an invokation.

		if ( (pos > 0) && (white_space.indexOf(last) < 0) && (next == '(') && (last != '(') )
		{
			token();
			buffer = '(';
			token('invoke');
			buffer = '';
			last = next;
			continue;
		}

		last = next;

		// Handle white space

		var is_ws = (white_space.indexOf(next) >= 0);

		if ( is_ws !== in_white_space )
		{
			if ( is_ws )
			{
				token();
				buffer = '';
				continue;
			}

			in_white_space = is_ws;
		}

		// Will this character break the sequence?

		if ( (next == '.') && /^[0-9]+$/.test(buffer.charAt(0)) )
		{
			buffer += '.';
			continue;
		}

		var test = next+code.charAt(pos);

		if ( breakers.indexOf(test) >= 0 )
		{
			token();
			buffer = test;
			token(test);
			pos++;
			continue;
		}
		else if ( breakers.indexOf(next) >= 0 )
		{
			token();
			buffer = next;
			token(next);
			continue;
		}

		// Add to buffer

		buffer += next;
	}

	token();

	return tokens;
}

// Parser

var operators = ['+', '-', '/', '\\', '\\\\', '*', '++', '!'];
var tokens = [];
var next_token = 0;
var errors = 0;

function error(err)
{
	console.log(err);
	errors++;
}

function consume_token()
{
	return tokens[next_token++];
}

function rewind_token()
{
	next_token--;
	if ( next_token < 0 )
		next_token = 0;
}

function peek_token()
{
	if ( next_token >= tokens.length )
		return null;

	return tokens[next_token];
}

function parse_object()
{
	// Object definition

	var entries = [];
	var key = null;
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( (type == 'string') || (type == 'symbol') )
		{
			if ( key )
			{
				rewind_token();
				left = parse_expression();
			}
			else
				key = token;
		}
		else if ( type == 'bind' )
		{
			// Accept a bind within a pattern match...

			left = {op:'bind', symbol:token[1]};
		}
		else if ( type == ':' )
		{
			if ( !key )
				error('Syntax error. Unexpected ";"');

			continue;
		}
		else if ( type == '}' )
		{
			// End of object definition

			break;
		}
		else if ( type == ',' )
		{
			if ( !left )
				error('Syntax error. Unexpected ","');

			// End of key/value pair

			entries.push({key:key, value:left});
			key = null;
		}
		else
		{
			if ( !key )
				error('Syntax error. Malformed object definition!');

			rewind_token();
			left = parse_expression();
		}
	}

	if ( left )
		entries.push({key:key, value:left});

	return {op:'object', entries:entries};
}

function parse_invoke_parameters()
{
	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid parameter list');

			params.push(left);
			left = null;
		}
		else if ( type == ')' )
		{
			// End of param list

			break;
		}
		else
		{
			rewind_token();
			left = parse_expression('invoke');
		}
	}

	if ( !token || token[0] != ')' )
		error('Syntax error: unclosed param list.')

	if ( left )
		params.push(left);

	return params;	
}

function parse_param_list()
{
	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid parameter definition list');

			params.push(left);
			left = null;
		}
		else if ( type == ')' )
		{
			// End of param list

			break;
		}
		else if ( type == 'symbol' )
		{
			left = token[1];
		}
		else
			error('Syntax error: Invalid parameter list');
	}

	if ( left )
		params.push(left);

	return params;
}

function parse_array()
{
	// Array definition

	var params = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == ',' )
		{
			if ( !left )
				error('Syntax error: Invalid array definition');

			params.push(left);
		}
		else if ( type == '|' )
		{
			if ( !left )
				error('Syntax error: "' + token[1] + '"');

			var right = parse_expression();
			return {op:'prepend', left:left, right:right};
		}
		else if ( type == 'bind' )
		{
			// Accept a bind within a pattern match...

			left = {op:'bind', symbol:token[1]};
		}
		else if ( type == ']' )
		{
			// End of array

			break;
		}
		else
		{
			rewind_token();
			left = parse_expression();
		}
	}

	if ( left )
		params.push(left);

	return {op:'list', entries:params};
}

function parse_recv()
{
	var patterns = [];
	var any = null;
	var timeout = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( type == 'timeout' )
		{
			if ( timeout )
				error('Multiple "timeout" branches in "recv"');

			var expr = parse_expression();
			var block = parse_block();

			timeout = {expression:expr, block:block};
		}
		else if ( type == 'end' )
		{
			// End of recv block

			break;
		}
		else
		{
			if ( type == 'or' )
			{
				if ( patterns.length == 0 )
					error('Syntax error: ' + type);
			}
			else
				rewind_token();

			var pattern = parse_expression('pattern');

			var block = parse_block();
			block.pattern = pattern;

			patterns.push(block);
		}
	}

	return {op:'recv', patterns:patterns, timeout:timeout, any:any};
}

function parse_expression(hint)
{
	var operator = null;
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];

		if ( !left )
		{
			if ( type == '{' )
				left = parse_object();
			else if ( type == '[' )
				left = parse_array();
			else if ( (type == 'number') || (type == 'string') || (type == 'symbol') || (type == 'true') || (type == 'false') )
				left = {op:'value', value:token};
			else if ( (type == 'bind') && (hint == 'pattern') )
				left = {op:'bind', symbol:token[1]};
			else if ( (type == 'self') || (type == 'parent') || (type == 'spawn') || (type == 'exit') )
				left = {op:type, value:type};
			else if ( type == 'def' )
			{
				var tmp = consume_token();
				var params = [];

				if ( (tmp[0] == '(') || (tmp[0] == 'invoke') )
					params = parse_param_list();
				else
					rewind_token();

				// Parse the function block

				var block = parse_block();
				left = {op:'def', params:params, block:block};

				// Consume the 'end' token

				var token = consume_token();
				if ( token[0] != 'end' )
					error('Syntax error: ' + token[0]);
			}
			else if ( type == 'recv' )
			{
				left = parse_recv();
			}
			else if ( type == '(' )
			{
				// Grouping of statements

				left = parse_expression('statement');
			}
			else
			{
				error('Syntax error in expression: "' + token[1] + '"');
			}

			if ( operator )
			{
				operator.right = left;
				left = operator;
				operator = null;
			}
		}
		else if ( type == ')' )
		{
			if ( hint == 'invoke' )
				rewind_token();

			break;
		}
		else if ( operators.indexOf(type) >= 0 )
		{
			if ( operator )
				error('Syntax error: ' + token[0]);

			operator = {op:type, left:left, right:null};
			left = null;
		}
		else if ( type == 'invoke' )
		{
			// Invocation...

			var right = parse_invoke_parameters();
			var name = null;

			// Allow for optimized call for invocations on
			// direct symbols.

			if ( (left.op == 'value') && (left.value[0] == 'symbol') )
			{
				name = left.value[1];
				left = null;
			}

			// Spawn is a special internal function that compiles
			// to a byte code, so check for that. Otherwise use
			// normal invoke command.

			var op = (left && (left.op == 'spawn')) ? 'spawn' : 'invoke';
			left = {op:op, name:name, left:left, params:right};
		}
		else if ( type == ':' )
		{
			if ( !left || (left.value[0] != 'symbol')  )
				error('Syntax error: Binding to non-symbol')

			var right = parse_expression();
			left = {op:'bind', symbol:left.value[1], right:right};
		}
		else
		{
			rewind_token();
			break;
		}
	}

	return left;
}

var block_depth = 0;

function parse_block()
{
	var statements = [];
	var left = null;

	var token;
	while ( token = consume_token() )
	{
		var type = token[0];
		var value = token[1];

		if ( (type == 'end') || (type == 'timeout') || (type == 'or') )
		{
			if ( block_depth == 0 )
			{
				error('Syntax error: ' + type);
				break;
			}

			rewind_token();
			break;
		}
		else
		{
			block_depth++;
			rewind_token();
			left = parse_expression();
			statements.push(left);
			left = null;
			block_depth--;
		}
	}

	if ( left )
		statements.push(left);

	if ( statements.length == 0 )
		return null;

	return {op:'block', statements:statements};
}

function parse(t)
{
	next_token = 0;
	tokens = t;
	
	var statements = [];

	var token;
	while ( next_token < tokens.length )
	{
		var block = parse_block();
		if ( block )
			statements.push(block);
	}

	return {op:'block', statements:statements};
}

// Compiler

var string_table = {};
var match_patterns = {};
var function_buffers = {0:{id:0, code:[], params:[]}};
var current_function = function_buffers[0];
var pattern_id = 1;
var function_id = 1;

function resolve_string(str)
{
	var k;
	for ( k in string_table )
	{
		var tmp = string_table[k];
		if ( str == tmp )
			return parseInt(k);
	}

	if ( !k )
		k = 0;

	var nid = ++k;
	string_table[nid] = str;

	return parseInt(nid);
}

function emit(code)
{
	current_function.code.push(code)
}

var stack_values = 0;

function compile_match_pattern(node, params, level)
{
	var pattern = node.pattern;
	var op = pattern.op;
	var type = null;
	var val = null;

	if ( op == 'value' )
	{
		var ttype = pattern.value[0];
		var tval = pattern.value[1];

		if ( ttype == 'string' )
		{
			val = resolve_string(tval);
			type = vm.pattern_types.STRING;
		}
		else if ( ttype == 'number' )
		{
			val = tval;
			type = vm.pattern_types.NUMBER;
		}
		else if ( ttype == 'symbol' )
		{
			val = resolve_string(tval);
			type = vm.pattern_types.VALUE;
		}
	}
	else if ( op == 'bind' )
	{
		val = resolve_string(pattern.symbol);
		type = vm.pattern_types.BIND;
		params.push(val);
	}
	else if ( op == 'list' )
	{
		val = [];

		var x;
		for ( x = 0; x < pattern.entries.length; x++ )
			val.push(compile_match_pattern({pattern:pattern.entries[x]}, params, level+1));

		type = vm.pattern_types.LIST;
	}
	else if ( op == 'object' )
	{
		val = [];

		var x;
		for ( x = 0; x < pattern.entries.length; x++ )
		{
			var entry = pattern.entries[x];
			val.push({key:resolve_string(entry.key[1]), pattern:compile_match_pattern({pattern:entry.value}, params, level+1)})
		}

		type = vm.pattern_types.OBJECT;
	}
	else
	{
		// Expression...

		compile({op:'block', statements:[pattern]}, []);
		type = vm.pattern_types.EXPR;
		stack_values++;
	}

	// Compile clause function

	var func = (level == 0) ? compile_function(node, params) : 0;
	var id = (level == 0) ? pattern_id++ : 0;

	var self = {id:id, type:type, value:val, params:params, func:func};
	if ( level == 0 )
		match_patterns[id] = self;

	return self;
}

function compile(node)
{
	if ( !node )
		return;

	var op = node.op;

	if ( op == 'bind' )
	{
		compile(node.right);

		emit([(current_function.id == 0 ? 'BINDG' : 'BIND'), resolve_string(node.symbol)]);
	}
	else if ( op == '!' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SEND']);
	}
	else if ( op == '+' )
	{
		compile(node.left);
		compile(node.right);
		emit(['ADD']);
	}
	else if ( op == '-' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SUB']);
	}
	else if ( op == '*' )
	{
		compile(node.left);
		compile(node.right);
		emit(['MUL']);
	}
	else if ( op == '/' )
	{
		compile(node.left);
		compile(node.right);
		emit(['DIV']);
	}
	else if ( op == 'mod' )
	{
		compile(node.left);
		compile(node.right);
		emit(['MOD']);
	}
	else if ( op == '\\' )
	{
		compile(node.left);
		compile(node.right);
		emit(['DEREF']);
	}
	else if ( op == '\\\\' )
	{
		compile(node.left);
		compile(node.right);
		emit(['SDEREF']);
	}
	else if ( op == 'exit' )
	{
		emit(['EXIT']);
	}
	else if ( op == 'self' )
	{
		emit(['SELF']);
	}
	else if ( op == 'parent' )
	{
		emit(['PARENT']);
	}
	else if ( op == 'spawn' )
	{
		node.params.forEach(function (e)
		{
			if ( e )
				compile(e);
		});

		emit(['SPAWN', node.params.length]);
	}
	else if ( op == 'block' )
	{
		node.statements.forEach(function (e)
		{
			if ( e )
				compile(e);
		});
	}
	else if ( op == 'invoke' )
	{
		if ( node.left )
			compile(node.left);

		node.params.forEach(function (e)
		{
			if ( e )
				compile(e);
		});

		if ( node.name )
			emit(['INVOKES', resolve_string(node.name), node.params.length]); // Invoke with named symbol
		else
			emit(['INVOKE', node.params.length]); // Invoke off stack
	}
	else if ( op == 'object' )
	{
		node.entries.forEach(function (e)
		{
			emit(['PUSHS', resolve_string(e.key[1])]);
			compile(e.value);
		});

		emit(['OBJECT',node.entries.length]);
	}
	else if ( op == 'list' )
	{
		node.entries.forEach(function (e)
		{
			compile(e);
		});

		emit(['LIST', node.entries.length]);
	}
	else if ( op == 'prepend' )
	{
		compile(node.left);
		compile(node.right);
		emit(['PREPEND']);
	}
	else if ( op == '++' )
	{
		compile(node.left);
		compile(node.right);
		emit('APPEND');
	}
	else if ( op == 'value' )
	{
		var token = node.value;
		var type = token[0];
		var value = token[1];

		if ( type == 'string' )
			emit(['PUSHS', resolve_string(value)]);
		else if ( type == 'number' )
			emit(['PUSHN', value]);
		else if ( type == 'symbol' )
			emit(['PUSHV', resolve_string(value)]);
		else if ( type == 'self' )
			emit(['SELF']);

	}
	else if ( op == 'recv' )
	{
		var timeout = -1;
		var clauses = -1;
		var any = -1;

		// Check for timeout clause

		if ( node.timeout )
		{
			timeout = compile_function(node.timeout.block);
			compile(node.timeout.expression);
		}

		// Compile pattern clauses

		var ins = ['RECV', timeout, 0, node.patterns.length];

		stack_values = 0;

		node.patterns.forEach(function (p)
		{
			var pattern = compile_match_pattern(p, [], 0);
			ins.push(pattern.id);
		});

		ins[2] = stack_values;

		// Setup instruction

		emit(ins);
	}
	else if ( op == 'def' )
	{
		// Compile the function

		var new_function_id = compile_function(node.block, node.params);
		
		// Push new function ID onto stack

		emit(['PUSHF',new_function_id]);
	}
}

function compile_function(block, params)
{
	if ( !params )
		params = [];

	// Map the parameter names to the string table

	var params = params.map(function (n)
	{
		if ( typeof n === 'number' )
			return n;

		return resolve_string(n);
	});

	// Generate new function entry

	var old_function = current_function;
	var new_function_id = function_id++;
	function_buffers[new_function_id] = {code:[], params:params, id:new_function_id};
	current_function = function_buffers[new_function_id];

	// Compile function code into new buffer

	if ( block )
		compile(block);

	// Reset to old buffer

	current_function = old_function;

	// Return function ID

	return new_function_id;	
}

// Assembler ///////////////////////////////////

var op_codes = {
	'NOOP': [vm.opcodes.NOOP, null],
	'BIND': [vm.opcodes.BIND, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'BINDG' : [vm.opcodes.BINDG, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'ADD': [vm.opcodes.ADD, null],
	'SUB': [vm.opcodes.SUB, null],
	'MUL': [vm.opcodes.MUL, null],
	'DIV': [vm.opcodes.DIV, null],
	'MOD': [vm.opcodes.MOD, null],
	'APPEND': [vm.opcodes.APPEND, null],
	'PREPEND': [vm.opcodes.PREPEND, null],
	'SPAWN': [vm.opcodes.SPAWN, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'OBJECT': [vm.opcodes.OBJECT, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'LIST': [vm.opcodes.LIST, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'PUSHN': [vm.opcodes.PUSHN, function (b,p,o) { b.writeDoubleLE(p[0], o); return 8; }],
	'PUSHS': [vm.opcodes.PUSHS, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'PUSHV': [vm.opcodes.PUSHV, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'PUSHF': [vm.opcodes.PUSHF, function (b,p,o) { b.writeInt32LE(p[0], o); return 4; }],
	'PNULL': [vm.opcodes.PNULL, null],
	'INVOKES': [vm.opcodes.INVOKES, function (b,p,o) { b.writeInt16LE(p[0], o); b.writeInt16LE(p[1], o+2); return 4; }],
	'INVOKE': [vm.opcodes.INVOKE, function (b,p,o) { b.writeInt16LE(p[0], o); return 2; }],
	'INVOKEI': [vm.opcodes.INVOKEI, function (b,p,o) { b.writeInt32LE(p[0], o); b.writeInt16LE(p[1],o+4); return 6; }],
	'EXIT': [vm.opcodes.EXIT, null],
	'SELF': [vm.opcodes.SELF, null],
	'DEREF' : [vm.opcodes.DEREF, null],
	'SDEREF' : [vm.opcodes.SDEREF, null],
	'PARENT': [vm.opcodes.PARENT, null],
	'SEND': [vm.opcodes.SEND, null],
	'RECV': [vm.opcodes.RECV, function (b,p,o)
	{
		b.writeInt32LE(p[0], o+0);
		b.writeInt32LE(p[1], o+4);
		b.writeInt32LE(p[2], o+8);

		o += 12;

		var x;
		for ( x = 0; x < p[2]; x++ )
		{
			b.writeInt32LE(p[3+x], o);
			o += 4;
		}

		return 12 + (x*4);
	}]
};

function assemble_pattern(pattern)
{
	var type = pattern.type;

	code_buffer.writeInt8(type, code_offset);
	code_offset += 1;

	code_buffer.writeInt32LE(pattern.func, code_offset);
	code_offset += 4;

	if ( type == vm.pattern_types.STRING )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.NUMBER )
	{
		code_buffer.writeDoubleLE(pattern.value, code_offset);
		code_offset += 8;
	}
	else if ( type == vm.pattern_types.VALUE )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.BIND )
	{
		code_buffer.writeInt32LE(pattern.value, code_offset);
		code_offset += 4;
	}
	else if ( type == vm.pattern_types.LIST )
	{
		var arr = pattern.value;

		code_buffer.writeInt32LE(arr.length, code_offset);
		code_offset += 4;

		var x;
		for ( x = 0; x < arr.length; x++ )
			assemble_pattern(arr[x]);
	}
	else if ( type == vm.pattern_types.OBJECT )
	{
		var entries = pattern.value;

		code_buffer.writeInt32LE(entries.length, code_offset);
		code_offset += 4;

		var x;
		for ( x = 0; x < entries.length; x++ )
		{
			var entry = entries[x];
			
			code_buffer.writeInt32LE(entry.key, code_offset);
			code_offset += 4;

			assemble_pattern(entry.pattern);
		}
	}
}

function assemble(func)
{
	// Output function ID

	code_buffer.writeInt32LE(func.id,code_offset);
	code_offset += 4;

	// Output instruction count

	code_buffer.writeInt32LE(func.code.length,code_offset);
	code_offset += 4;

	// Output parameter count

	code_buffer.writeInt8(func.params.length,code_offset);
	code_offset++;

	// Output parameter names (Referenced in string table)

	func.params.forEach(function (p)
	{
		code_buffer.writeInt32LE(p,code_offset);
		code_offset += 4;
	});

	// Output the list of instructions

	func.code.forEach(function (instruction)
	{
		var name = instruction[0];
		var params = instruction.slice(1);

		var opcode = op_codes[name];
		if ( !opcode )
			error('Invalid instruction: ' + name);

		code_buffer.writeInt8(opcode[0],code_offset);
		code_offset++;

		if ( opcode[1] )
			code_offset += opcode[1](code_buffer,params,code_offset);
	});
}

// Compiles a block of code and runs the
// opcodes on the VM.

function xy_compile(input)
{
	code_buffer = new Buffer(4096*4, 'binary');
	code_offset = 0;
	errors = 0;

	code_buffer.writeInt32LE(vm.signature, code_offset);
	code_offset += 4;

	// Output code tree

	var tree = parse(tokenize(input.toString()));

	if ( DEBUG_MODE )
	{
		console.log(util.inspect(tree, false, null));
		console.log('');
	}

	compile(tree);

	// Output string table

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('String table')
		console.log('--------------------------');
		console.log(util.inspect(string_table, false, null));
		console.log('');
	}

	code_buffer.writeInt32LE(Object.keys(string_table).length,code_offset);
	code_offset += 4;

	for ( var k in string_table )
	{
		var s = string_table[k];

		code_buffer.writeInt32LE(k, code_offset);
		code_offset += 4;

		code_buffer.writeInt32LE(s.length,code_offset);
		code_offset += 4;

		code_buffer.write(s,code_offset,s.length,'binary');
		code_offset += s.length;
	}

	// Output match pattern table

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Match pattern table')
		console.log('--------------------------');
		console.log(util.inspect(match_patterns, false, null));
		console.log('');
	}

	code_buffer.writeInt32LE(Object.keys(match_patterns).length, code_offset);
	code_offset += 4;

	for ( var k in match_patterns )
	{
		var pattern = match_patterns[k];

		// Output pattern ID

		code_buffer.writeInt32LE(pattern.id, code_offset);
		code_offset += 4;

		// Output the pattern

		assemble_pattern(pattern);
	}

	// Assemble the functions to the buffer

	code_buffer.writeInt32LE(Object.keys(function_buffers).length,code_offset);
	code_offset += 4;

	for ( var k in function_buffers )
	{
		var func = function_buffers[k];

		if ( DEBUG_MODE )
		{
			console.log('--------------------------');
			console.log('Function ' + k);
			console.log('--------------------------');	
			console.log(util.inspect(func, false, null));
			console.log('');
		}

		assemble(func);
	}

	// Add some bindings for testing

	vm.hook('inspect', function (process, params)
	{
		var processed = params.map(function (param)
		{
			if ( param == null )
				return '<<null>>';
			else if ( typeof param == 'number' )
				return '<<number:' + param + '>>';
			else if ( typeof param == 'string' )
				return '<<string:"' + param + '">>';
			else if ( typeof param == 'object' )
			{
				if ( param.__function )
					return '<<function:' + param.__function.id + '>>';
				else if ( param.__process )
					return '<<process:' + param.__process.id + '>>';
				else if ( param.__native )
					return '<<native:' + param.__name + '>>';
				else if ( param.__object )
					return '<<object:' + Object.keys(param.__object).join(',') + '>>';
				else if ( param.__list )
					return '<<list:' + param.__list.length + '>>';
			}
			else
				return '<<unknown>>';
		});

		console.log(processed.join(' '));
	});

	vm.hook('log', function (process, params)
	{
		var mapped = params.map(function (param)
		{

			if ( param && (typeof param === 'object') )
			{
				if ( param.__function )
					return '<<function:' + param.__function.id + '>>';
				else if ( param.__process )
					return '<<process:' + param.__process.id + '>>';
				else if ( param.__native )
					return '<<native:' + param.__name + '>>';
				else if ( param.__object )
					return '<<object:' + Object.keys(param.__object).join(',') + '>>';
				else if ( param.__list )
					return '<<list:' + param.length + '>>';
			}
			else if ( param == null )
				return '<<null>>'
			else
				return param;
		});

		console.log(mapped.join(' '));
	});
}

// External hook function

function xy_hook(name, func)
{
	vm.hook(name, func);
}

// Boots the VM and starts running code

function xy_boot()
{
	if ( errors )
		return false;
	
	vm.load(code_buffer, 0, code_offset);
	vm.boot();

	return true;
}

// Run from command line if available. Otherwise run from DOM

if ( process.argv.length > 2 )
{
	xy_compile(fs.readFileSync(process.argv[2]));
	return xy_boot();
}
else if ( window )
{
	window.addEventListener('load', function() {
		var elements = document.querySelectorAll('script[type="text/xy"]');
		var content = '';

		var x;
		for ( x = 0; x < elements.length; x++ )
			content += elements[x].innerText;

		xy_compile(content);
	}, false);
}

window.xy_boot = xy_boot;
window.xy_hook = xy_hook;

}).call(this,require('_process'),require("buffer").Buffer)
},{"./vm.js":2,"_process":9,"buffer":4,"fs":3,"util":11}],2:[function(require,module,exports){
(function (process){

var util = require('util');

// String table

var strings = {};

// Function table

var functions = {};

// Table of patterns to match on

var match_patterns = {};

// List of processes

var processes = {};
var process_count = 0;
var active_processes = [];
var next_pid = 0;

// Constants

var ROOT_TIMER_INTERVAL = 2147483647;

var MAX_MAILBOX_SIZE = 128;
var MAX_RUN_CYCLES = 64;

var PROCESS_TERMINATED = 0;
var PROCESS_ACTIVE = 1;
var PROCESS_RECIEVING = 2;

var DEBUG_MODE = (process.argv.indexOf('-d') >= 0);

// List of pattern match types

var pattern_types = {
	BIND : 0,
	NUMBER : 1,
	STRING : 2,
	LIST : 3,
	OBJECT : 4,
	VALUE : 5,
	EXPR : 6
};

// The list of opcodes

var opcodes = {
	NOOP : 0,
	BIND : 1,
	BINDG : 2,
	ADD : 3,
	SUB : 4,
	MUL : 5,
	DIV : 6,
	MOD : 7,
	APPEND : 8,
	PREPEND : 9,
	SPAWN : 10,
	OBJECT : 11,
	LIST : 12,
	PUSHN : 13,
	PUSHS : 14,
	PUSHV : 15,
	PUSHF : 16,
	PNULL : 17,
	INVOKES : 18,
	INVOKEI : 19,
	INVOKE : 20,
	EXIT : 21,
	SELF : 22,
	PARENT : 23,
	SEND : 24,
	RECV : 25,
	DEREF : 26,
	SDEREF : 27,

	LT : 28,
	GT : 29,
	GTE : 30,
	LTE : 31,
	EQ : 32,
	NE : 33,
	CMP : 34,

	_MAX_ : 35
};

// Global bindings. These will always be available
// in scope to all functions. This acts as a way to provide
// hooks for 'native' non-VM functions via the hook(name,f)
// export.

var global_bindings = {};

// Functions for reading opcodes

var opcode_readers = {};

opcode_readers[opcodes.BIND] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.BINDG] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.OBJECT] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.LIST] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.PUSHN] = function (b,p,o)
{
	p.push(b.readDoubleLE(o));
	return 8;
};

opcode_readers[opcodes.PUSHS] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.PUSHV] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.PUSHF] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	return 4;
};

opcode_readers[opcodes.INVOKES] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	p.push(b.readInt16LE(o+2));
	return 4;
};

opcode_readers[opcodes.INVOKEI] = function (b,p,o)
{
	p.push(b.readInt32LE(o));
	p.push(b.readInt16LE(o+4));
	return 6;
};

opcode_readers[opcodes.INVOKE] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.SPAWN] = function (b,p,o)
{
	p.push(b.readInt16LE(o));
	return 2;
};

opcode_readers[opcodes.RECV] = function (b,p,o)
{
	p.push(b.readInt32LE(o+0));
	p.push(b.readInt32LE(o+4));
	p.push(b.readInt32LE(o+8));

	o += 12;

	var x;
	for ( x = 0; x < p[2]; x++ )
	{
		p.push(b.readInt32LE(o));
		o += 4;
	}

	return 12 + (x*4);
};

// Loads a virtual machine module

var code_buffer = null;
var code_offset = 0;

function load_pattern(id)
{
	var pattern = {};

	var type = code_buffer.readInt8(code_offset);
	code_offset += 1;

	var func = code_buffer.readInt32LE(code_offset);
	code_offset += 4;

	var value = null;

	if ( type == pattern_types.STRING )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.NUMBER )
	{
		value = code_buffer.readDoubleLE(code_offset);
		code_offset += 8;
	}
	else if ( type == pattern_types.VALUE )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.BIND )
	{
		value = strings[code_buffer.readInt32LE(code_offset)];
		code_offset += 4;
	}
	else if ( type == pattern_types.LIST )
	{
		var length = code_buffer.readInt32LE(code_offset);
		code_offset += 4;

		value = [];

		var x;
		for ( x = 0; x < length; x++ )
			value.push(load_pattern(0));
	}
	else if ( type == pattern_types.OBJECT )
	{
		var length = code_buffer.readInt32LE(code_offset);
		code_offset += 4;

		value = [];

		var x;
		for ( x = 0; x < length; x++ )
		{
			var strid = code_buffer.readInt32LE(code_offset);
			code_offset += 4;

			value.push([strings[strid], load_pattern(0)]);		
		}
	}

	return {id:id, type:type, func:func, value:value};
}

function opcode_valid(opcode)
{
	return ( (opcode >= 0) && (opcode < opcodes._MAX_) );
}

function load(buffer, buffer_offset, buffer_length)
{
	code_offset = buffer_offset;
	code_buffer = buffer;

	// Check that buffer has valid signature

	var sig = buffer.readInt32LE(code_offset);
	code_offset += 4;

	if ( sig != module.exports.signature )
	{
		if ( DEBUG_MODE )
			console.log('Invalid module signature: ' + sig);

		return false;
	}

	// Load the string table

	var string_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	var x;
	for ( x = 0; x < string_count; x++ )
	{
		var id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var length = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var str = buffer.toString('binary', code_offset, code_offset+length);
		code_offset += length;

		strings[id] = str;
	}

	// Output strings

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded string table')
		console.log('--------------------------');
		console.log(util.inspect(strings, false, null));
		console.log('');
	}

	// Load the match pattern table

	var match_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	var x;
	for ( x = 0; x < match_count; x++ )
	{
		var id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var pattern = load_pattern(id);
		match_patterns[id] = pattern;
	}

	// Output patterns

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded match pattern table');
		console.log('--------------------------');
		console.log(util.inspect(match_patterns, false, null));
		console.log('');
	}

	// Read in the functions

	var function_count = buffer.readInt32LE(code_offset);
	code_offset += 4;

	for ( x = 0; x < function_count; x++ )
	{
		var function_id = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var instruction_count = buffer.readInt32LE(code_offset);
		code_offset += 4;

		var param_count = buffer.readInt8(code_offset);
		code_offset++;

		var params = [];

		var p;
		for ( p = 0; p < param_count; p++ )
		{
			params.push(strings[buffer.readInt32LE(code_offset)]);
			code_offset += 4;
		}

		var instructions = [];

		var i;
		for ( i = 0; i < instruction_count; i++ )
		{
			var opcode = buffer.readInt8(code_offset);
			if ( !opcode_valid(opcode) )
			{
				console.log('Invalid opcode:',opcode);
				return false;
			}

			code_offset++;

			var instr = {opcode:opcode, params:null};

			var f = opcode_readers[opcode];
			if ( f )
			{
				instr.params = [];
				code_offset += f(buffer,instr.params,code_offset);
			}

			instructions.push(instr);
		}

		functions[function_id] = {id: function_id, instructions: instructions, params:params};
		functions[function_id].__function = functions[function_id];
	}

	if ( DEBUG_MODE )
	{
		console.log('--------------------------');
		console.log('Loaded function table');
		console.log('--------------------------');
		console.log(util.inspect(functions, false, null));
		console.log('');
	}

	// Patch match patterns with actual function references
	// instead of IDs. This is just to avoid an additional
	// map lookup at runtime.

	for ( var k in match_patterns )
	{
		var pattern = match_patterns[k];
		if ( pattern.func )
			pattern.func = functions[pattern.func];
	}

	// All good!

	return true;
}

// Terminates a process and sends a termination message
// to it's parent/supervisor.

function terminate(process, reason)
{
	// Do nothing if process already terminated

	if ( process.state == PROCESS_TERMINATED )
		return;

	// If receiving, make sure timer is cancelled

	var timer = (process.receiving ? process.receiving.timer : null);
	if ( timer )
		clearTimeout(timer);

	// Flag process as terminated and ensure
	// receive state is nullified

	process.state = PROCESS_TERMINATED;
	process.receiving = null;

	// Terminate child processes

	var children = process.children;
	process.children = null;

	var x;
	for ( x = 0; x < children.length; x++ )
	{
		var child = children[x];
		terminate(child, null);
	}

	// Clear child list

	children.length = 0;

	// Remove from process map

	delete processes[process.id];

	// Send message to parent if available

	var parent = process.parent;
	if ( parent )
	{
		if ( reason )
			send(parent, object({terminated:{__process:process}, reason:reason}));

		process.parent = null;
	}

	// Reduce process count

	if ( (process.id == 0) || DEBUG_MODE )
		console.log('Process terminated:', process.id, 'reason:',(reason ? util.inspect(reason, false, null) : 'parent_terminated'));

	process_count--;
}

// Wakes up a process and places it in the active list

function wake(process)
{
	// If already awake, do nothing

	if ( (process.state == PROCESS_ACTIVE) || (process.state == PROCESS_TERMINATED) )
		return;

	// Stop a timer if we have one

	if ( process.receiving.timer )
		clearTimeout(process.receiving.timer);

	// Set process to active state and clear
	// receive data.

	process.state = PROCESS_ACTIVE;
	process.receiving = null;

	// Push the process back onto the active list

	active_processes.push(process);

	if ( DEBUG_MODE )
		console.log('Process awakened:',process.id);

	// Run the VM (If not already running)

	run();
}

// Called by the VM when a process times out while receiving
// messages.

function timeout(process)
{
	// Invoke the timeout function on the process
	// and reset it's state to active.

	invoke(process, process.receiving.timeout, []);
	wake(process);

	return true;
}

// Suspends a process and removes it from the active list
// The process will wait until it has received a message
// matching the provided pattern.

function receive(process, timeout_value, timeout_function, clauses, values)
{
	if ( process.state == PROCESS_RECIEVING )
		return;

	// Ensure timeout value is valid (Must be a number)

	if ( timeout_value !== null )
	{
		if ( typeof timeout_value != 'number' )
		{
			terminate(process, error_message('invalid_timeout_value', timeout_value));
			return false;
		}
	}

	// Try immediately matching to process mailbox
	// If the message matches we'll immediately
	// wake up the process.

	var mailbox = process.mailbox;
	var receiving = {
		timeout: timeout_function,
		clauses: clauses,
		values: values
	};

	var x;
	for ( x = 0; x < mailbox.length; x++ )
	{
		var msg = mailbox[x];

		var res = match_message(receiving, msg);
		if ( res )
		{
			mailbox.splice(x,1);
			invoke(process, res[0], res[1], true);
			return true;
		}
	}

	// Activate timer if necessary

	if ( timeout_value )
	{
		receiving.timer = setTimeout(function ()
		{
			if ( DEBUG_MODE )
				console.log('Process timed out on recieve:', process.id);

			timeout(process);
		}, timeout_value);
	}

	// Put into receiving state

	process.state = PROCESS_RECIEVING;
	process.receiving = receiving;

	// All done!

	return true;
}

function match_pattern_recurse(pattern_params, clause, value, scope)
{
	var ctype = clause.type;
	var cval = clause.value;

	switch ( ctype )
	{
		case pattern_types.STRING:
			if ( value === clause.value )
				return clause.func;
			return false;

		case pattern_types.NUMBER:
			if ( value === clause.value )
				return clause.func;
			return false;

		case pattern_types.BIND:
			pattern_params.push(value);
			if ( scope == 0 )
				return clause.func;
			break;

		case pattern_types.LIST:
			var array = value.__list;
			if ( (array === undefined) || (cval.length != array.length) )
				return false

			var length = cval.length;

			var x;
			for ( x = 0; x < length; x++ )
			{
				if ( match_pattern_recurse(pattern_params, cval[x], array[x], scope+1) === false )
					return false;
			}

			if ( scope == 0 )
				return clause.func;

			break;

		case pattern_types.OBJECT:
			var map = value.__object;
			if ( (map === undefined) || (value.__size != cval.length) )
				return false;

			var length = cval.length;

			var x;
			for ( x = 0; x < length; x++ )
			{
				var entry = cval[x];
				var key = entry[0];
				var val = map[key];

				if ( (val === undefined) || (match_pattern_recurse(pattern_params, entry[1], val, scope+1) === false) )
					return false;
			}

			if ( scope == 0 )
				return clause.func;

			break;
	}
}

function match_pattern(clause, value)
{
	var pattern_params = [];

	var func = match_pattern_recurse(pattern_params, clause, value, 0);
	if ( func )
		return [func, pattern_params];

	return null;
}

// Matches a message to a process mailbox

function match_message(receiving, value)
{
	// Match value with the list of pattern clauses

	var clauses = receiving.clauses;
	var result = null;

	var x;
	for ( x = 0; x < clauses.length; x++ )
	{
		var res = match_pattern(clauses[x], value);
		if ( res )
			return res;
	}

	return null;
}

// Sends a message to the given process ID

function send(process, data)
{
	// Make sure process is alive

	if ( process.state == PROCESS_TERMINATED )
		return false;

	// Make sure mailbox size is not too big

	if ( process.mailbox.length >= MAX_MAILBOX_SIZE )
	{
		terminate(process, object({error:'mailbox_maxed_out'}));
		return false;
	}

	// Match process mailbox with message data
	// If no match can be made it will end
	// up in the mailbox.

	if ( process.state == PROCESS_RECIEVING )
	{
		var response = match_message(process.receiving, data);
		if ( response )
		{
			invoke(process, response[0], response[1], true);
			wake(process);
			return true;
		}
	}

	// Add message to mailbox to be handled later

	process.mailbox.push(data);
	return true;
}

// Spawns a new process and returns the PID

function spawn(parent, function_val, params)
{
	// Make sure params are correct

	if ( !params )
		params = [];

	// Spawn the process with new state

	var pid = next_pid++;
	var process = {
		id: pid,
		params: params,
		frames: [],
		stack: [],
		mailbox: [],
		parent: parent,
		children: [],
		receiving: null,
		state: PROCESS_ACTIVE
	};

	// Add new process to children

	if ( parent )
		parent.children.push(process);

	// Invoke the function on the process

	if ( !invoke(process, function_val, params) )
		return null;

	// Record process ID and new process count

	processes[pid] = process;
	process_count++;

	active_processes.push(process);

	if ( DEBUG_MODE )
		console.log('Spawned process:',pid);

	return {__process:process};
}

// Resolves a symbol by going backwards through
// the stack frames.

function resolve_binding(symbol, frames)
{
	// Try the stack frame

	var x;
	for ( x = frames.length-1; x >= 0; x-- )
	{
		var bindings = frames[x].bindings;
		var tmp = bindings[symbol];
		if ( tmp !== undefined )
			return tmp;
	}

	// Now try the global bindings

	var tmp = global_bindings[symbol];
	if ( tmp !== undefined )
		return tmp;

	// No luck, return undefined...

	return undefined;
}

// Maps function parameters to a binding space

function map_params(bindings, fparams, params)
{
	var x = 0;
	for ( x = 0; x < fparams.length; x++ )
	{
		var str = fparams[x];
		bindings[str] = (x < params.length) ? params[x] : null;
	}
}

// Maps function parameters to a binding space
// and returns a key name if there is a collision

function map_params_safe(bindings, fparams, params)
{
	var x = 0;
	for ( x = 0; x < fparams.length; x++ )
	{
		var str = fparams[x];
		if ( str in bindings )
			return str;

		bindings[str] = (x < params.length) ? params[x] : null;
	}

	return null;
}

function inner_function(frame, f)
{
	for ( var k in frame.bindings )
	{
		var b = frame.bindings[k];
		if ( b.__function == f )
			return true;
	}

	return false;
}

// Invokes a function within a process
// TODO: Need a way to implement tail recursion

function invoke(process, func, params, reuse_bindings)
{
	// Ensure we have a valid function value

	if ( !func )
	{
		terminate(process, error_message('invalid_function', null));
		return false;
	}

	// Native function? If so call the function and
	// push value onto stack immediately.

	var n = func.__native;
	if ( n !== undefined )
	{
		var r = n(process, params);
		if ( r !== undefined )
			process.stack.push(r);

		return true;
	}

	// Get function reference from value

	var f = func.__function;
	if ( f === undefined )
	{
		terminate(process, error_message('non_function_invoke', null));
		return false;
	}

	var bindings = null;

	var frames = process.frames;
	if ( frames.length > 0 )
	{
		// Pop all frames that are completed
		// to save memory (Tail call optimization)

		var current = frames[frames.length-1];

		while ( current && (current.code_ptr >= current.func.instructions.length) )
		{
			// Tail recursion?

			if ( current.func == f )
			{
				current.code_ptr = 0;
				map_params(current.bindings, f.params, params);
				return true;
			}

			// Internal recursion?

			if ( inner_function(current, f) )
			{
				current.func = f;
				current.code_ptr = 0;
				map_params(current.bindings, f.params, params);
				return true;				
			}

			// Pop frame and move to next...

			process.frames.pop();
			current = frames[frames.length-1];
		}

		// Reuse existing bindings if needed

		if ( reuse_bindings )
		{
			bindings = current.bindings;

			var error_key = map_params_safe(bindings, f.params, params);
			if ( error_key )
			{
				terminate(process, error_message('already_binded', error_key));
				return false;
			}
		}
	}

	// Setup new binding map or bind to existing
	// map space if one was provided.

	if ( bindings == null )
	{
		bindings = {};
		map_params(bindings, f.params, params);
	}

	// Push new stack frame

	var stack_frame = {
		func: f,
		bindings: bindings,
		code_ptr: 0
	};

	// Push frame onto process frame stack

	process.frames.push(stack_frame);

	// All good!

	return true;
}

function error_message(cause, info)
{
	return object({error:object({cause:cause, info:info})});
}

// Evaluates a process based on it's state

function eval_process(process)
{
	// Make sure process is active

	if ( process.state != PROCESS_ACTIVE )
		return;

	// Get current process state

	var stack = process.stack;
	var frames = process.frames;
	var current_frame = frames[frames.length-1];
	var func = current_frame.func;
	var code_ptr = current_frame.code_ptr;
	var instructions = func.instructions;
	var bindings = current_frame.bindings;

	// Execute the instructions

	var x;
	for ( x = code_ptr; x < instructions.length; x++ )
	{
		var instr = instructions[x];

		switch ( instr.opcode )
		{
			case opcodes.NOOP:
				break;

			case opcodes.PNULL:
				stack.push(null);
				break;

			case opcodes.PUSHN:
				stack.push(instr.params[0]);
				break;

			case opcodes.PUSHS:
				stack.push(strings[instr.params[0]]);
				break;

			case opcodes.PUSHV:
				var symbol = strings[instr.params[0]];
				var val = resolve_binding(symbol, frames);
				if ( val === undefined )
				{
					terminate(process, error_message('undefined_symbol', symbol));
					return;
				}
				stack.push(val);
				break;

			case opcodes.PUSHF:
				var f = functions[instr.params[0]];
				stack.push({__function:f});
				break;

			case opcodes.ADD:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2+n1);
				break;

			case opcodes.SUB:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2-n1);
				break;

			case opcodes.MUL:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2*n1);
				break;

			case opcodes.DIV:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2/n1);
				break;

			case opcodes.MOD:
				var n1 = stack.pop();
				var n2 = stack.pop();
				stack.push(n2%n1);
				break;

			case opcodes.DEREF:
				var key = stack.pop();
				var obj = stack.pop();
				if ( obj.__object === undefined )
				{
					if ( obj.__list === undefined )
					{
						terminate(process, error_message('non_object_dereference', obj));
						return;
					}

					val = obj.__list[key];
					stack.push(val === undefined ? null : val);
					break;
				}
				var val = obj.__object[key];
				stack.push(val === undefined ? null : val);
				break;

			case opcodes.SDEREF:
				var key = stack.pop();
				var obj = stack.pop();
				if ( obj.__object === undefined )
				{
					if ( obj.__list === undefined )
					{
						terminate(process, error_message('non_object_dereference', obj));
						return;
					}

					var val = obj.__list[key]
					if ( val === undefined )
					{
						terminate(process, error_message('invalid_dereference_index', key));
						return;						
					}
					break;
				}
				var val = obj.__object[key];
				if ( val === undefined )
				{
					terminate(process, error_message('invalid_dereference_key', key));
					return;
				}
				stack.push(val);
				break;			

			case opcodes.INVOKES:
				var symbol = strings[instr.params[0]];
				var param_count = instr.params[1];
				var f = resolve_binding(symbol, frames);
				if ( f === undefined )
				{
					terminate(process, error_message('undefined_symbol', symbol));
					return;
				}
				var params = stack.slice(stack.length-param_count);
				process.stack = stack.slice(0, stack.length-param_count);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.INVOKE:
				var param_count = instr.params[0];
				var f = stack[stack.length-param_count-1];
				var params = stack.slice(stack.length-param_count)
				process.stack = stack.slice(0, stack.length-param_count-1);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.INVOKEI:
				var f = functions[instr.params[0]];
				var param_count = instr.params[1];
				var params = stack.slice(stack.length-param_count);
				process.stack = stack.slice(0, stack.length-param_count);
				current_frame.code_ptr = x+1;
				invoke(process, f, params);
				return;

			case opcodes.SPAWN:
				var param_count = instr.params[0];
				var f = stack[stack.length-param_count];
				var pid = spawn(process, f, stack.slice(stack.length-(param_count-1)));
				process.stack = stack.slice(0, stack.length-param_count-1);
				stack = process.stack;
				stack.push(pid);
				break;

			case opcodes.SELF:
				stack.push({__process:process});
				break;

			case opcodes.PARENT:
				var parent = process.parent;
				stack.push(parent ? {__process:parent} : null);
				break;

			case opcodes.BIND:
				var symbol = strings[instr.params[0]];
				var value = stack.pop();
				if ( bindings[symbol] !== undefined )
				{
					terminate(process, error_message('already_binded', symbol));
					return;
				}
				bindings[symbol] = value;
				break;

			case opcodes.BINDG:
				var symbol = strings[instr.params[0]];
				var value = stack.pop();
				if ( global_bindings[symbol] !== undefined )
				{
					terminate(process, error_message('already_binded', symbol));
					return;
				}
				global_bindings[symbol] = value;
				break;

			case opcodes.EXIT:
				terminate(process, 'exit');
				return;

			case opcodes.LIST:
				var length = instr.params[0];
				var array = stack.splice(stack.length-length);
				stack.push({__list:array});
				break;

			case opcodes.OBJECT:
				var entries = instr.params[0]*2;
				var params = stack.splice(stack.length-entries);
				var obj = {};

				var t;
				for ( t = 0; t < entries; t+=2 )
				{
					var key = params[t+0];
					var value = params[t+1];
					obj[key] = value;
				}

				stack.push(object(obj, instr.params[0]));
				break;

			case opcodes.SEND:
				var message = stack.pop();
				var procv = stack.pop();

				var proc = (procv ? procv.__process : null);
				if ( !proc )
				{
					terminate(process, error_message('invalid_send', procv));
					return;
				}

				send(proc, message);
				break;

			case opcodes.LT:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 < v1);
				break;

			case opcodes.GT:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 > v1);
				break;

			case opcodes.LTE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 <= v1);
				break;

			case opcodes.GTE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 >= v1);
				break;

			case opcodes.EQ:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 == v1);
				break;

			case opcodes.NE:
				var v1 = stack.pop();
				var v2 = stack.pop();
				stack.push(v2 != v1);
				break;

			case opcodes.RECV:
				var timeout = instr.params[0]; 				// Timeout function ID
				var stack_values = instr.params[1];			// Number of stack values
				var clause_count = instr.params[2]; 				// Number of clauses
				var tmp;
				
				// Get the clauses

				var clauses = [];

				for ( tmp = 0; tmp < clause_count; tmp++ )
					clauses.push(match_patterns[instr.params[3+tmp]]);

				// Pop expression stack values

				var values = [];

				for ( tmp = 0; tmp < stack_values; tmp++ )
					values.push(stack.pop());

				var timeout_value = (timeout >= 0) ? stack.pop() : null;
				var timeout_function = (timeout >= 0) ? functions[timeout] : null;

				receive(process, timeout_value, timeout_function, clauses, values);
				current_frame.code_ptr = x+1;
				return;

			default:
				terminate(process, error_message('invalid_instruction', instr.opcode));
				return;
		}
	}

	if ( DEBUG_MODE )
		console.log('Function completed with stack:', process.stack, "frames:", frames);

	// Function completed. Pop current stack frame

	frames.pop();

	// Done with process? If so, terminate it.

	if ( frames.length == 0 )
		terminate(process, 'completed');
}

function object(obj, size)
{
	return {__object:obj, __size:((size !== undefined) ? size : Object.keys(obj).length)};
}

// Evaluates active processes and builds a new
// active process list after evaluation.
//
// TODO: Come up with better way to
// handle new active process list?

function eval()
{
	// Reset active list. When evaluation is complete,
	// active_processes will have only newly spawned
	// processes from this cycle.

	var current_active = active_processes;
	active_processes = [];

	// Step 1: Evaluate active processes

	current_active.forEach(function (process)
	{
		eval_process(process);
	});

	// Step 2: Determine which processes are still active
	// by filtering out inactive processes after the
	// evaluation has completed.

	current_active.forEach(function (process)
	{
		if ( process.state == PROCESS_ACTIVE )
			active_processes.push(process);
	});

	// All done!

	return true;
}

// Evaluates function 0 to initialize state
// end start proces

var running = false;
var root_timer = null;

function run()
{
	// Can only run once at a time...

	if ( running )
		return false;

	running = true;

	// Keep evaluating until we reached the maximum number of cycles
	// or there are no more active processes.

	var cycles = 0;
	while ( (active_processes.length > 0) && ((cycles++) < MAX_RUN_CYCLES) )
		eval();

	// Did all processes terminate?

	if ( process_count == 0 )
	{
		if ( DEBUG_MODE )
			console.log('Run completed');

		running = false;
		shutdown();

		return true;
	}

	// Did we reach max cycles? If so schedule for another
	// run later.

	if ( cycles >= MAX_RUN_CYCLES )
	{
		if ( DEBUG_MODE )
			console.log('Max cycles reached:',cycles);

		setTimeout(run, 0);
	}

	// All done!

	running = false;
	return true;
}

// Shuts down the VM

function shutdown()
{
	if ( running )
		return false;

	// Clear the root timer

	if ( root_timer )
	{
		clearInterval(root_timer);
		root_timer = null;
	}

	// Hard terminate all processes
	// by just resetting the process map
	// and active list.

	strings = {};
	functions = {};
	match_patterns = {};
	active_processes = [];
	process_count = 0;
	processes = {};

	// All done!

	return true;
}

// Bootstraps the VM and begins running

function boot()
{
	// Are we already booted?

	if ( process_count != 0 )
		return false;

	// Create a root timer. This will keep the process
	// alive in case we are using node. Can also use
	// it for monitoring VM stats in future...

	root_timer = setInterval(function () {}, ROOT_TIMER_INTERVAL);

	// Find the root function

	var f = functions[0];
	if ( f === undefined )
		return false;

	// Spawn a root process for the function

	spawn(null, {__function:f}, []);

	// Start running the processes

	run();

	// All good!

	return true;
}

// Registers a function within the main process scope

function hook(name, func)
{
	if ( (typeof name !== 'string') || (typeof func !== 'function') )
		return false;

	global_bindings[name] = {__native:func, __name:name};
	return true;
}

// Exports

module.exports.signature = 0xBEEF;
module.exports.opcodes = opcodes;
module.exports.pattern_types = pattern_types;

module.exports.hook = hook;
module.exports.object = object;

module.exports.send = send;
module.exports.load = load;
module.exports.boot = boot;

}).call(this,require('_process'))
},{"_process":9,"util":11}],3:[function(require,module,exports){

},{}],4:[function(require,module,exports){
(function (global){
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */

'use strict'

var base64 = require('base64-js')
var ieee754 = require('ieee754')
var isArray = require('isarray')

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50
Buffer.poolSize = 8192 // not used by this implementation

var rootParent = {}

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.foo = function () { return 42 }
    return arr.foo() === 42 && // typed array instances can be augmented
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */
function Buffer (arg) {
  if (!(this instanceof Buffer)) {
    // Avoid going through an ArgumentsAdaptorTrampoline in the common case.
    if (arguments.length > 1) return new Buffer(arg, arguments[1])
    return new Buffer(arg)
  }

  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    this.length = 0
    this.parent = undefined
  }

  // Common case.
  if (typeof arg === 'number') {
    return fromNumber(this, arg)
  }

  // Slightly less common case.
  if (typeof arg === 'string') {
    return fromString(this, arg, arguments.length > 1 ? arguments[1] : 'utf8')
  }

  // Unusual.
  return fromObject(this, arg)
}

// TODO: Legacy, not needed anymore. Remove in next major version.
Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function fromNumber (that, length) {
  that = allocate(that, length < 0 ? 0 : checked(length) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < length; i++) {
      that[i] = 0
    }
  }
  return that
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') encoding = 'utf8'

  // Assumption: byteLength() return value is always < kMaxLength.
  var length = byteLength(string, encoding) | 0
  that = allocate(that, length)

  that.write(string, encoding)
  return that
}

function fromObject (that, object) {
  if (Buffer.isBuffer(object)) return fromBuffer(that, object)

  if (isArray(object)) return fromArray(that, object)

  if (object == null) {
    throw new TypeError('must start with number, buffer, array or string')
  }

  if (typeof ArrayBuffer !== 'undefined') {
    if (object.buffer instanceof ArrayBuffer) {
      return fromTypedArray(that, object)
    }
    if (object instanceof ArrayBuffer) {
      return fromArrayBuffer(that, object)
    }
  }

  if (object.length) return fromArrayLike(that, object)

  return fromJsonObject(that, object)
}

function fromBuffer (that, buffer) {
  var length = checked(buffer.length) | 0
  that = allocate(that, length)
  buffer.copy(that, 0, 0, length)
  return that
}

function fromArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Duplicate of fromArray() to keep fromArray() monomorphic.
function fromTypedArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  // Truncating the elements is probably not what people expect from typed
  // arrays with BYTES_PER_ELEMENT > 1 but it's compatible with the behavior
  // of the old Buffer constructor.
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array) {
  array.byteLength // this throws if `array` is not a valid ArrayBuffer

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(array)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromTypedArray(that, new Uint8Array(array))
  }
  return that
}

function fromArrayLike (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Deserialize { type: 'Buffer', data: [1,2,3,...] } into a Buffer object.
// Returns a zero-length buffer for inputs that don't conform to the spec.
function fromJsonObject (that, object) {
  var array
  var length = 0

  if (object.type === 'Buffer' && isArray(object.data)) {
    array = object.data
    length = checked(array.length) | 0
  }
  that = allocate(that, length)

  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
} else {
  // pre-set for values that may exist in the future
  Buffer.prototype.length = undefined
  Buffer.prototype.parent = undefined
}

function allocate (that, length) {
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that.length = length
  }

  var fromPool = length !== 0 && length <= Buffer.poolSize >>> 1
  if (fromPool) that.parent = rootParent

  return that
}

function checked (length) {
  // Note: cannot use `length < kMaxLength` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (subject, encoding) {
  if (!(this instanceof SlowBuffer)) return new SlowBuffer(subject, encoding)

  var buf = new Buffer(subject, encoding)
  delete buf.parent
  return buf
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  var i = 0
  var len = Math.min(x, y)
  while (i < len) {
    if (a[i] !== b[i]) break

    ++i
  }

  if (i !== len) {
    x = a[i]
    y = b[i]
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'binary':
    case 'base64':
    case 'raw':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) throw new TypeError('list argument must be an Array of Buffers.')

  if (list.length === 0) {
    return new Buffer(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; i++) {
      length += list[i].length
    }
  }

  var buf = new Buffer(length)
  var pos = 0
  for (i = 0; i < list.length; i++) {
    var item = list[i]
    item.copy(buf, pos)
    pos += item.length
  }
  return buf
}

function byteLength (string, encoding) {
  if (typeof string !== 'string') string = '' + string

  var len = string.length
  if (len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'binary':
      // Deprecated
      case 'raw':
      case 'raws':
        return len
      case 'utf8':
      case 'utf-8':
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length // assume utf8
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false

  start = start | 0
  end = end === undefined || end === Infinity ? this.length : end | 0

  if (!encoding) encoding = 'utf8'
  if (start < 0) start = 0
  if (end > this.length) end = this.length
  if (end <= start) return ''

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'binary':
        return binarySlice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.
Buffer.prototype._isBuffer = true

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return 0
  return Buffer.compare(this, b)
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset) {
  if (byteOffset > 0x7fffffff) byteOffset = 0x7fffffff
  else if (byteOffset < -0x80000000) byteOffset = -0x80000000
  byteOffset >>= 0

  if (this.length === 0) return -1
  if (byteOffset >= this.length) return -1

  // Negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = Math.max(this.length + byteOffset, 0)

  if (typeof val === 'string') {
    if (val.length === 0) return -1 // special case: looking for empty string always fails
    return String.prototype.indexOf.call(this, val, byteOffset)
  }
  if (Buffer.isBuffer(val)) {
    return arrayIndexOf(this, val, byteOffset)
  }
  if (typeof val === 'number') {
    if (Buffer.TYPED_ARRAY_SUPPORT && Uint8Array.prototype.indexOf === 'function') {
      return Uint8Array.prototype.indexOf.call(this, val, byteOffset)
    }
    return arrayIndexOf(this, [ val ], byteOffset)
  }

  function arrayIndexOf (arr, val, byteOffset) {
    var foundIndex = -1
    for (var i = 0; byteOffset + i < arr.length; i++) {
      if (arr[byteOffset + i] === val[foundIndex === -1 ? 0 : i - foundIndex]) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === val.length) return byteOffset + foundIndex
      } else {
        foundIndex = -1
      }
    }
    return -1
  }

  throw new TypeError('val must be string, number or Buffer')
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new Error('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; i++) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) throw new Error('Invalid hex string')
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function binaryWrite (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  // legacy write(string, encoding, offset, length) - remove in v0.13
  } else {
    var swap = encoding
    encoding = offset
    offset = length | 0
    length = swap
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'binary':
        return binaryWrite(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function binarySlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; i++) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; i++) {
      newBuf[i] = this[i + start]
    }
  }

  if (newBuf.length) newBuf.parent = this.parent || this

  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('buffer must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('value is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; i++) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; i++) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('index out of range')
  if (offset < 0) throw new RangeError('index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; i--) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; i++) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function fill (value, start, end) {
  if (!value) value = 0
  if (!start) start = 0
  if (!end) end = this.length

  if (end < start) throw new RangeError('end < start')

  // Fill 0 bytes; we're done
  if (end === start) return
  if (this.length === 0) return

  if (start < 0 || start >= this.length) throw new RangeError('start out of bounds')
  if (end < 0 || end > this.length) throw new RangeError('end out of bounds')

  var i
  if (typeof value === 'number') {
    for (i = start; i < end; i++) {
      this[i] = value
    }
  } else {
    var bytes = utf8ToBytes(value.toString())
    var len = bytes.length
    for (i = start; i < end; i++) {
      this[i] = bytes[i % len]
    }
  }

  return this
}

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; i++) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; i++) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"base64-js":5,"ieee754":6,"isarray":7}],5:[function(require,module,exports){
;(function (exports) {
  'use strict'

  var lookup = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

  var Arr = (typeof Uint8Array !== 'undefined')
    ? Uint8Array
    : Array

  var PLUS = '+'.charCodeAt(0)
  var SLASH = '/'.charCodeAt(0)
  var NUMBER = '0'.charCodeAt(0)
  var LOWER = 'a'.charCodeAt(0)
  var UPPER = 'A'.charCodeAt(0)
  var PLUS_URL_SAFE = '-'.charCodeAt(0)
  var SLASH_URL_SAFE = '_'.charCodeAt(0)

  function decode (elt) {
    var code = elt.charCodeAt(0)
    if (code === PLUS || code === PLUS_URL_SAFE) return 62 // '+'
    if (code === SLASH || code === SLASH_URL_SAFE) return 63 // '/'
    if (code < NUMBER) return -1 // no match
    if (code < NUMBER + 10) return code - NUMBER + 26 + 26
    if (code < UPPER + 26) return code - UPPER
    if (code < LOWER + 26) return code - LOWER + 26
  }

  function b64ToByteArray (b64) {
    var i, j, l, tmp, placeHolders, arr

    if (b64.length % 4 > 0) {
      throw new Error('Invalid string. Length must be a multiple of 4')
    }

    // the number of equal signs (place holders)
    // if there are two placeholders, than the two characters before it
    // represent one byte
    // if there is only one, then the three characters before it represent 2 bytes
    // this is just a cheap hack to not do indexOf twice
    var len = b64.length
    placeHolders = b64.charAt(len - 2) === '=' ? 2 : b64.charAt(len - 1) === '=' ? 1 : 0

    // base64 is 4/3 + up to two characters of the original data
    arr = new Arr(b64.length * 3 / 4 - placeHolders)

    // if there are placeholders, only get up to the last complete 4 chars
    l = placeHolders > 0 ? b64.length - 4 : b64.length

    var L = 0

    function push (v) {
      arr[L++] = v
    }

    for (i = 0, j = 0; i < l; i += 4, j += 3) {
      tmp = (decode(b64.charAt(i)) << 18) | (decode(b64.charAt(i + 1)) << 12) | (decode(b64.charAt(i + 2)) << 6) | decode(b64.charAt(i + 3))
      push((tmp & 0xFF0000) >> 16)
      push((tmp & 0xFF00) >> 8)
      push(tmp & 0xFF)
    }

    if (placeHolders === 2) {
      tmp = (decode(b64.charAt(i)) << 2) | (decode(b64.charAt(i + 1)) >> 4)
      push(tmp & 0xFF)
    } else if (placeHolders === 1) {
      tmp = (decode(b64.charAt(i)) << 10) | (decode(b64.charAt(i + 1)) << 4) | (decode(b64.charAt(i + 2)) >> 2)
      push((tmp >> 8) & 0xFF)
      push(tmp & 0xFF)
    }

    return arr
  }

  function uint8ToBase64 (uint8) {
    var i
    var extraBytes = uint8.length % 3 // if we have 1 byte left, pad 2 bytes
    var output = ''
    var temp, length

    function encode (num) {
      return lookup.charAt(num)
    }

    function tripletToBase64 (num) {
      return encode(num >> 18 & 0x3F) + encode(num >> 12 & 0x3F) + encode(num >> 6 & 0x3F) + encode(num & 0x3F)
    }

    // go through the array every three bytes, we'll deal with trailing stuff later
    for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
      temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
      output += tripletToBase64(temp)
    }

    // pad the end with zeros, but make sure to not forget the extra bytes
    switch (extraBytes) {
      case 1:
        temp = uint8[uint8.length - 1]
        output += encode(temp >> 2)
        output += encode((temp << 4) & 0x3F)
        output += '=='
        break
      case 2:
        temp = (uint8[uint8.length - 2] << 8) + (uint8[uint8.length - 1])
        output += encode(temp >> 10)
        output += encode((temp >> 4) & 0x3F)
        output += encode((temp << 2) & 0x3F)
        output += '='
        break
      default:
        break
    }

    return output
  }

  exports.toByteArray = b64ToByteArray
  exports.fromByteArray = uint8ToBase64
}(typeof exports === 'undefined' ? (this.base64js = {}) : exports))

},{}],6:[function(require,module,exports){
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],7:[function(require,module,exports){
var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};

},{}],8:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],9:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],10:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],11:[function(require,module,exports){
(function (process,global){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./support/isBuffer":10,"_process":9,"inherits":8}]},{},[1]);
